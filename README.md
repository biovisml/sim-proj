# README #

The Similarity Projection algorithm re-examines the problem of similarity measures for alignment-free sequence comparison, and introduce a new method which we term \emph{Similarity Projection}. Similarity Projection offers markedly enhanced sensitivity -- comparable to alignment based methods -- while retaining the scalability characteristic of alignment-free approaches. 

### Who am I? ###

I am Lawrence Buckingham, an Associate Lecturer and PhD candidate in the School of Electrical Engineering and Computer Science of Queensland University of Technology in Brisbane, Australia. 
*	email: l.buckingham@qut.edu.au

### What is here? ###

This source repository has been initially populated to coincide with publication of a paper which has been accepted at the thirteenth IEEE eScience Conference, held in Auckland, New Zealand from 24 � 27 October 2017. The paper is: "Similarity Projection: A geometric measure for comparison of biological sequences", by Lawrence Buckingham, Timothy Chappell, James M. Hogan, and Shlomo Geva

Please note that code in this repository is work in progress towards my PhD. The software is in a transitional state, reflecting the on-going research journey.

There are three exectuable programs, each of which resides in a folder of its own.
*	BuildKmerCodebook: parses a sequence dataset in FASTA format, selects a designated number of prototype k-mers, and clusters the dataset to these prototypes to produce a VQ codebook suitable for use by KmerRank.
*	KmerRank: given a database of sequences in FASTA format, a VQ codebook, and a query dataset in FASTA format, this program ranks the top N best hits from the database for each sequence in the query dataset.
*	trec-eval-tc: an implementation of the rank scoring algorithm developed by Tim Chappell to enable processing of ranking files larger than 2GB.

Makefiles provided are suitable for use under 64-bit Linux environments, and have been tested with a range of recent GCC versions >= 5.2. They also work well in Windows, using the Cygwin Posix emulation environment. Two makefiles are provided for each. The regular makefile produces a standard dynamically linked executable, while makefile.static produces a statically linked executable which can be deployed to a machine that does not have a sufficiently modern version of GCC installed. Statically linked exectuables have been tested under Ubuntu and RedHat Enterprise (versions to be supplied).

### Still to come ###

The following additional material will be available in the very near future:

*	The datasets (SP100000 and SP500000) described int the results section of the paper, along with k-mer codebooks used for the experiments.
*	A detailed tutorial on the use of the tools.

### Ongoing work ### 

*	I am presently working on a more sensitive codebook construction process to enable better detection of homologs for extremely under-represented protein families.
*	the software will be applied to case studies involving other sequence formats, including DNA, RNA, and other non-cellular sequence data.
