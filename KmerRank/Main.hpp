#pragma once

#include "QutBio.h"
#include "Homologs.hpp"
#include "KmerDistanceCache.hpp"

#include <set>
#include <iomanip>
#include <numeric>

using namespace QutBio;
using namespace std;

#undef max

namespace KmerQuery {


	class Main {
	public:
		/// <summary> Main class contains the program entry point: Run().
		/// </summary>
		static void Run(int argc, char ** argv) {
			Args arguments(argc, argv);

			arguments.Show();

			if ( arguments.IsDefined("numThreads") ) {
				int numThreads;
				if ( arguments.Get("numThreads", numThreads) ) {
					omp_set_num_threads(numThreads);
					cerr << "OMP thread count set to " << numThreads << endl;
				}
			}
			else {
				omp_set_num_threads(1);
			}

			try {
				Process(arguments);
			}
			catch ( Exception e ) {
				string message = e.what();

				if ( message.find("Safe to ignore") != 0 ) {
					throw e;
				}
			}
		}

	private:

		static void WriteRankingsToFile(
			vector<Ranking> & rankings,
			size_t maxRecords,
			ofstream & rankingFile
			) {
			FastaSequence * querySeq = rankings[0].query;
			const string & queryId = querySeq->Id();
			const size_t N = std::min(rankings.size(), maxRecords);

			for ( size_t i = 0; i < N; i++ ) {
				rankingFile << rankings[i] << '\n';
			}
		}

		static void Process(Args arguments) {
			KmerSequenceRanker_Params parms{arguments};

			cout << "--seed " << parms.seed << endl;

			BlosumDifferenceFunction dist(parms.matrix);
			KmerDistanceCache2 cache(parms.alphabet, &dist);

			bool filesOk = true;

			if ( !File::Exists(parms.dbFile) ) {
				filesOk = false;
				(cerr << "Database file " << parms.dbFile << " cannot be opened to read." << endl).flush();
			}

			if ( !File::Exists(parms.queryFile) ) {
				filesOk = false;
				(cerr << "Database file " << parms.dbFile << " cannot be opened to read." << endl).flush();
			}

			if ( !filesOk ) {
				exit(1);
			}

			PointerList<FastaSequence> db;

			FastaSequence::ReadSequences(parms.dbFile, parms.idIndex, parms.classIndex, db);
			PreprocessDataset(db, parms.kmerLength, 'x', parms.alphabet, cache.CharsPerWord());

			(cerr << "subject dataset contains " << db.Length() << " sequences." << endl).flush();

			PointerList<FastaSequence> query_;

			if ( parms.queryFile != parms.dbFile ) {
				FastaSequence::ReadSequences(parms.queryFile, parms.idIndex, parms.classIndex, query_);
				PreprocessDataset(query_, parms.kmerLength, 'x', parms.alphabet, cache.CharsPerWord());

				(cerr << "Query dataset contains " << query_.Length() << " sequences." << endl).flush();
			}

			PointerList<FastaSequence> &query = query_.Length() > 0 ? query_ : db;
			vector<FastaSequence *> querySubset;

			if ( parms.sampleSize <= 0 || parms.sampleSize >= query.Length() ) {
				querySubset = query.Items();
			}
			else {
				UniformRealRandom rand(parms.seed);
				Selector want(rand, parms.sampleSize, query.Length());

				for ( auto seq : query ) {
					if ( want.SelectThis() ) {
						querySubset.push_back(seq);
					}
				}

				assert_equal((size_t) parms.sampleSize, querySubset.size());
			}

			(cerr << "Query subset contains " << querySubset.size() << " sequences." << endl).flush();

			// Write out the query IDs if requested.
			if ( parms.queryIdFile.length() > 0 ) {
				auto f = fopen(parms.queryIdFile.c_str(), "wb");

				for ( auto qSeq : querySubset ) {
					fprintf(f, "%s\n", qSeq->Id().c_str());
				}

				fclose(f);
			}

			ofstream rankingFile(parms.rankingFile);

			Action1<vector<Ranking> &> postProcessRankings = [&](vector<Ranking> & rankings) {
				WriteRankingsToFile(rankings, parms.maxRecords, rankingFile);
			};

			KmerCodebook * codebook = 0;

			if ( parms.codebookFile.size() > 0 ) {
				FILE * f = fopen(parms.codebookFile.c_str(), "r");
				if ( f ) {
					codebook = new KmerCodebook(cache, parms.kmerLength, db, f);
					fclose(f);
				}
				else {
					(cerr << "Codebook " << parms.codebookFile << " cannot be opened for reading." << endl).flush();
					exit(1);
				}
			}

			auto initRanker = [&](KmerSequenceRanker &ranker) {
				ranker.SetCodebook(codebook);
		};

#if defined(USE_BIG_CACHE)
			auto dist(RawKmerDistanceFunctionFactory::Factory(parms.dist, parms.matrixId));
			KmerDistanceCache cachedCalculator(parms.alphabet, dist);
#endif

			KmerSequenceRanker ranker(
				parms.matrix,
				parms.kmerMode,
				parms.fragMode,
				parms.alphabet,
				parms.fragLength,
				parms.kmerLength,
				parms.thresholdDistance,
				parms.defaultDistance,
				parms.pushKmerDistances,
				cache,
				parms.skip,
				parms.maxRecords
				);
			initRanker(ranker);

			ranker.SetQueryComplete(postProcessRankings);
			ranker.RunJob(querySubset, db.Items());

			rankingFile.close();
	}

		/**
		 *	<summary>
		 *		If necessary, pads all sequences out to be as long as the word-size, k.
		 *		Encodes the sequences to produce packed word matrices (if we insist that
		 *		k be even, then we can collapse the matrices down to a single array, which
		 *		would be nice.
		 *	</summary>
		 */
		static void PreprocessDataset(PointerList<FastaSequence> & db, size_t kmerLength, char padding, Alphabet * alpha, size_t charsPerWord) {
#pragma omp parallel for
			for ( int i = 0; i < (int) db.Length(); i++ ) {
				FastaSequence * seq = db[i];
				string & s = seq->Sequence();

				while ( s.size() < kmerLength ) {
					s.push_back(padding);
				}

				seq->Encode(alpha, kmerLength, charsPerWord);
			}
		}
};

}


