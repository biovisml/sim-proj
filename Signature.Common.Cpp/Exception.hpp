#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <string>

using namespace std;

namespace QutBio {

	class Exception : public std::runtime_error {
	private:
		string file;
		int line;

	public:
		Exception( string message_, string file, int line )
			: runtime_error( message_ ), file( file ), line( line ) {}

		const string & File() { return file; };

		int Line() { return line; }
	};

	class KeyNotFoundException : public Exception {
	private:
		string key;

	public:
		KeyNotFoundException( string message_, string file, int line )
			: Exception( message_, file, line ), key( "Key not found" ) {}

		KeyNotFoundException( string message_, string key_, string file, int line )
			: Exception( message_, file, line ), key( key_ ) {}

		string & Key() { return key; }
	};

	class NotImplementedException : public Exception {
	public:
		NotImplementedException( string file, int line )
			: Exception( "Not implemented.", file, line ) {}
	};
}

#define FileAndLine __FILE__, __LINE__

