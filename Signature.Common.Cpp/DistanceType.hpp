#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include "EnumBase.hpp"

#include <string>
#include <mutex>
using namespace std;

namespace QutBio {
	class DistanceType : public EnumBase {
	private:
		DistanceType( string literal, int value ) : EnumBase( literal, value ) {}
		static mutex m;
		
	public:  
		static DistanceType * Blosum() {
			unique_lock < mutex > lck{m};
#pragma warning( disable : 4640 )
			static DistanceType value( "Blosum", 0 );
#pragma warning( default : 4640 )
			return &value;
		}
		
		static DistanceType * UngappedEdit() {
			unique_lock < mutex > lck{m};
			static DistanceType value( "UngappedEdit", 1 );
			return &value;
		}

		static DistanceType * BlosumSimilarity() {
			unique_lock < mutex > lck{m};
			static DistanceType value( "BlosumSimilarity", 2 );
			return &value;
		}

		/**
		 *	<summary>
		 *		A custom distance which is defined by a similarity matrix loaded at run time.
		 *	</summary>
		 */
		static DistanceType * Custom() {
			unique_lock < mutex > lck{m};
			static DistanceType value( "Custom", 3 );
			return &value;
		}

		static Array<EnumBase *> Values() {
			Array<EnumBase *> result( 4 );
			result[0] = Blosum();
			result[1] = UngappedEdit();
			result[2] = BlosumSimilarity();
			result[3] = Custom();
			return result;
		}
	};
}
