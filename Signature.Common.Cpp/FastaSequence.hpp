#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <string>
#include <unordered_map>
#include <vector>

#include "CharMap.hpp"
#include "PrecisionRecallRecord.hpp"
#include "PointerList.hpp"
#include "Selector.hpp"
#include "Util.hpp"
#include "TrecEvalRecord.hpp"

using namespace std;

namespace QutBio {
	struct Ranking;
	
	using EncodingMatrix = vector<vector<KmerWord>>;

	class FastaSequence {
	private:
		string id;
		string defLine;
		string sequence;
		string classLabel;
		vector<uint64_t> embedding;
		size_t length;

	public:

		// I know, this is messy as anything, but pending a total rewrite, this is what we have for now.
		// Position, rowMinima, colMinima, fragDistAuxData and hit are used by database sequences to store 
		// information used in the collection-wide kmer sweep.
		size_t position;
		vector<Distance> rowMinima;
		vector<Distance> colMinima;
		void * fragDistAuxData = 0;
		function<void(void *p)> deleteFragDistAuxData;
		vector<FastaSequence *> homologs;
		vector<int> classNumbers;

		// When I'm using the cached 2-mer and/or 3-mer score tables, this is the packed image of the kmers in the collection.
		EncodingMatrix encoding;
		
		size_t charsPerWord;
		const EncodedKmer(FastaSequence::*getEncodedKmer)(size_t pos);

		const string & Id() const { return id; }
		const string & DefLine() const { return defLine; }
		const string & ClassLabel() const { return classLabel; }
		const vector<uint64_t> Embedding() { return embedding; }

		string & Sequence() { return sequence; }

		void Sequence(const string & value) {
			sequence.clear();

			for ( auto ch : value ) {
				if ( ch == '-' || isspace( ch ) ) continue;

				sequence.push_back(tolower(ch));
			}

			length = sequence.size();
		}

		static unordered_map<string, int> & ClassNumberRegistry() {
			static unordered_map<string, int> classNumberRegistry;
			return classNumberRegistry;
		}

		static int GetClassId(const string & classLabel) {
			auto&& classNumberRegistry = ClassNumberRegistry();
			int classNumber = -1;

			auto existingRecord = classNumberRegistry.find(classLabel);

			if ( existingRecord == classNumberRegistry.end() ) {
				classNumber = (int) classNumberRegistry.size();
				classNumberRegistry[classLabel] = classNumber;
			}
			else {
				classNumber = existingRecord->second;
			}

			return classNumber;
		}

		FastaSequence(const string & id, const string & classLabel, const string & defLine, const string & sequence) :
			id(id), classLabel(classLabel), defLine(defLine) {

			Sequence(sequence);

			if ( classLabel.size() > 0 ) {
				auto splitClassLabels = String::Split(classLabel, ';');

				for ( string & classLabel : splitClassLabels ) {
					int classNumber = GetClassId(classLabel);
					classNumbers.push_back(classNumber);
				}
			}
		}

		virtual ~FastaSequence() {
			if ( fragDistAuxData ) deleteFragDistAuxData(fragDistAuxData);
		}

		/// <summary>
		/// Parses a FASTA file and extracts a set of sequences. The fields of the
		/// definition line are assumed to be separated by '|' symbols, with an identifier
		/// in one field and the class label in another.
		/// <para>
		///		The id number may be a GI number, or something else such as a binding site ID or a
		///		application specific id number. This may be any string except one containing a pipe '|'
		///		symbol.
		/// </para>
		/// <para>
		///		The class label is an arbitrary string, which once again, cannot contain embedded pipe 
		///		'|' symbols.
		/// </para>
		/// </summary>
		/// <param name="sequenceFile">The name of the file containing the sequences to be loaded.</param>
		/// <param name="idIndex">The index of the pipe-separated field (counting from 0) that contains the id number.</param>
		/// <param name="classIndex">The index of the pipe-separated field (counting from 0) that contains the class label.</param>
		/// <returns>A dictionary containing the sequences from the file, indexed by their id numbers.</returns>

		static void ReadSequences(
			istream & reader,
			int idIndex,
			int classIndex,
			PointerList<FastaSequence> & sequences
			) {
			string currentLine;
			string currentDefLine;
			string currentSequence;

			auto update = [&]() {
				if ( currentSequence.size() > 0 ) {
					vector<string> parts = String::Split(currentDefLine, "|");

					if ( idIndex >= parts.size() ) {
						throw Exception("Index Out Of Bounds: idIndex", FileAndLine);
					}

					string & gi = parts[idIndex];

					if ( classIndex >= 0 && classIndex >= parts.size() ) {
						throw Exception("Index Out Of Bounds: classIndex", FileAndLine);
					}

					static string empty = "";
					string & classLabel = classIndex >= 0 ? parts[classIndex] : empty;
					sequences.Add([&]() { return new FastaSequence(gi, classLabel, currentDefLine, currentSequence); });
				}
			};

			auto reset = [&]() {
				currentSequence = "";
				currentDefLine = currentLine.substr(1);
			};

			while ( !(reader.fail() || reader.bad() || reader.eof()) ) {
				getline(reader, currentLine);
				String::TrimInPlace(currentLine);

				if ( currentLine[0] == '>' ) {
					update();
					reset();
				}
				else {
					currentSequence += String::Trim(currentLine);
				}
			}

			if ( !reader.eof() ) {
				throw Exception("Error reading from stream.", __FILE__, __LINE__);
			}

			update();

			// Console.Error( String.Format( "{0} definition lines read.", sequences.Count ) );
		}

		/// <summary>
		/// Parses a FASTA file and extracts a set of sequences. The fields of the
		/// definition line are assumed to be separated by '|' symbols, with an identifier
		/// in one field and the class label in another.
		/// <para>
		///		The id number may be a GI number, or something else such as a binding site ID or a
		///		application specific id number. This may be any string except one containing a pipe '|'
		///		symbol.
		/// </para>
		/// <para>
		///		The class label is an arbitrary string, which once again, cannot contain embedded pipe 
		///		'|' symbols.
		/// </para>
		/// </summary>
		/// <param name="sequenceFile">The name of the file containing the sequences to be loaded.</param>
		/// <param name="idIndex">The index of the pipe-separated field (counting from 0) that contains the id number.</param>
		/// <param name="classIndex">The index of the pipe-separated field (counting from 0) that contains the class label.</param>
		/// <returns>A dictionary containing the sequences from the file, indexed by their id numbers.</returns>

		static void ReadSequences(
			const string & fileName,
			int idIndex,
			int classIndex,
			PointerList<FastaSequence> & sequences
			) {
			ifstream reader(fileName);
			ReadSequences(reader, idIndex, classIndex, sequences);
			reader.close();
		}

		/**
		*	<summary>
		*		Pads each sequence out to the designated minimum length
		*	</summary>
		*	<param name="db">A list of sequences to process.</param>
		*	<param name="minLength">The required minimum length.</param>
		*	<param name="padding">The symbol used to pad the sequence.</param>
		*/

		static void PadSequences(PointerList<FastaSequence> & db, int minLength, char padding) {
			for ( auto seq : db ) {
				seq->Pad(minLength, padding);
			}
		}

		/**
		*	<summary>
		*		Pads the sequence out to the designated minimum length
		*	</summary>
		*	<param name="minLength">The required minimum length.</param>
		*	<param name="padding">The symbol used to pad the sequence.</param>
		*/

		void Pad(size_t minLength, char padding) {
			while ( sequence.size() < minLength ) {
				sequence.push_back(padding);
			}

			length = sequence.size();
		}

		/// <summary> Serialises this FastaSequence to text.
		/// </summary>
		/// <returns></returns>

		string ToString() {
			ostringstream out;
			out << defLine << endl << sequence << endl;
			return out.str();
		}

		/**
		*	<summary>
		*		Returns true iff a designated sequence appears in the
		*		list of homologs of this sequence.
		*		(Needless to say, this is not to be used in search/classification.)
		*	</summary>
		*	<param name="other">A candidate homolog.</param>
		 */
		bool IsHomolog(const FastaSequence * other) {
			if ( homologs.size() > 0 ) {
				return find(homologs.begin(), homologs.end(), other) != homologs.end();
			}
			else {
				for ( auto i : classNumbers ) {
					for ( auto j : other->classNumbers ) {
						if ( i == j ) return true;
					}
				}
			}

			return false;
		}

		bool IsHomologAny(const vector<const FastaSequence *> & others) {
			for ( auto other : others ) {
				if ( IsHomolog(other) ) {
					return true;
				}
			}

			return false;
		}

		void SetEmbedding(const CharMap & charMap) {
			embedding.resize(sequence.size());

			for ( size_t i = 0; i < sequence.size(); i++ ) {
				embedding[i] = charMap.bits[sequence[i]].lo;
			}
		}

		/**
		 *	<summary>
		 *		Gets a normalised histogram with the counts of each symbol.
		 *	</summary>
		 *	<param name="db">
		 *		Reference to a pointer-list of FastaSequence objects in which symbol occurrences are to be calculated.
		 *	</param>
		 *	<param name="histogram">
		 *		Reference to a histogram that will be overwritten with the symbol probabilities observed in the database.
		 *	</param>
		 */

		static void GetSymbolHistogram(const vector<FastaSequence *> & db, Histogram<char> & histogram) {
			histogram.data.clear();

			for ( FastaSequence * seq : db ) {
				histogram.AddRange(seq->Sequence());
			}

			histogram.Normalise();
		}

		/***
		*	<summary>
		*		Uses the supplied selector to choose zero or more kmers from this sequence and pass them back for processing.
		*	</summary>
		*	<param name="kmerLength">The size of the desired kmers.</param>
		*	<param name="selector">A reference to a Selector that determines which, if any, kmers are processed.</param>
		*	<param name="process">A function that is invoked once per selected kmer to process the selection.</param>
		***/

		void SelectKmers(size_t kmerLength, Selector & selector, function<void(FastaSequence * seq, size_t pos, size_t length)> process) {
			if ( length < kmerLength ) return;

			size_t n = length - kmerLength + 1;

			for ( size_t i = 0; i < n; i++ ) {
				if ( selector.SelectThis() ) {
					process(this, i, kmerLength);
				}
			}
		}

		/***
		*	<summary>
		*		Iterates over the kmers in this sequence and passes them back for processing.
		*	</summary>
		*	<param name="kmerLength">The size of the desired kmers.</param>
		*	<param name="process">A function that is invoked once per selected kmer to process the selection.</param>
		***/

		void SelectKmers(size_t kmerLength, function<void(FastaSequence * seq, size_t pos, size_t length)> process) {
			size_t n = KmerCount(kmerLength);

			for ( size_t i = 0; i < n; i++ ) {
				process(this, i, kmerLength);
			}
		}

		class Index : public unordered_map<string, FastaSequence *> {
		public:

			/**
			 *	<summary>
			 *		Gets an index which can be used to look up sequences by Id.
			 *	</summary>
			 *	<param name="db">
			 *		Reference to a pointer-list of FastaSequence objects in which symbol occurrences are to be calculated.
			 *	</param>
			 *	<param name="index">
			 *		Reference to a hash table that will be populated with the lookup table. Any previous contents in this
			 *		data structure will be erased.
			 *	</param>
			 */
			Index(const vector<FastaSequence *> & dataset) {
				for ( auto seq : dataset ) {
					(*this)[seq->Id()] = seq;
				}

				assert_equal(dataset.size(), this->size());
			}
		};

		/**
		 *	<summary>
		 *		Returns the number of kmers required to tile the longest sequence in a dataset.
		 *	</summary>
		 *	<param name="db">A list of sequences.</param>
		 *	<param name="kmerLength">The kmer length.</param>
		 */
		static size_t GetMaxKmerCount(PointerList<FastaSequence> & db, size_t kmerLength) {
			size_t maxKmerCount = 0;

			db.ForEach([&maxKmerCount, kmerLength](FastaSequence * seq) {
				size_t K = seq->length - kmerLength + 1;

				if ( K > maxKmerCount ) {
					maxKmerCount = K;
				}
			});

			return maxKmerCount;
		}

		/**
		 *	<summary>
		 *		Returns the total number of kmers in a dataset.
		 *	</summary>
		 *	<param name="db">A list of sequences.</param>
		 *	<param name="kmerLength">The kmer length.</param>
		 */
		static size_t GetTotalKmerCount(PointerList<FastaSequence> & db, size_t kmerLength) {
			size_t totalKmerCount = 0;

			for ( auto seq : db ) {
				totalKmerCount += seq->KmerCount(kmerLength);
			}

			return totalKmerCount;
		}

		/**
		 *	<summary>
		 *		Packs each kmer in the present sequence into a list of KmerWord arrays with the
		 *		designated length and density. Each kmer is then represented as one row in the
		 *		encoding member of this sequence.
		 *	</summary>
		 */

		void Encode(Alphabet * alphabet, size_t kmerLength, size_t charsPerWord) {
			this->charsPerWord = charsPerWord;
			this->getEncodedKmer =
				charsPerWord == 1 ? &FastaSequence::GetEncodedKmer1 :
				charsPerWord == 2 ? &FastaSequence::GetEncodedKmer2 :
				charsPerWord == 3 ? &FastaSequence::GetEncodedKmer3 :
				&FastaSequence::GetEncodedKmerGeneral;
			size_t kmerCount = KmerCount(kmerLength);
			size_t codeSize = Alphabet::WordsPerKmer(kmerLength, charsPerWord);
			alphabet->Encode(sequence.c_str(), length, kmerLength, charsPerWord, encoding);
		}

		/**
		 *	<summary>
		 *	Returns the number of complete kmers of length K in the
		 *	</summary>
		 */
		size_t KmerCount(size_t K) {
			return length >= K ? length + 1 - K : 0;
		}

		const EncodedKmer GetEncodedKmer(size_t pos) {
			return (this->*getEncodedKmer)(pos);
		}

		const EncodedKmer GetEncodedKmerGeneral(size_t pos) {
			return &encoding[pos%charsPerWord][pos / charsPerWord];
		}

		const EncodedKmer GetEncodedKmer1(size_t pos) {
			return &encoding[0][pos];
		}

		const EncodedKmer GetEncodedKmer2(size_t pos) {
			return &encoding[pos % 2][pos / 2];
		}

		const EncodedKmer GetEncodedKmer3(size_t pos) {
			return &encoding[pos % 3][pos / 3];
		}

		size_t Length() const {
			return length;
		}

	};
}
