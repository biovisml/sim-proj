#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif


#include <algorithm>
#include <vector>
#include <iostream>
#include <string.h>

using namespace std;

#include "db.hpp"
#include "Delegates.hpp"
#include "Exception.hpp"

namespace QutBio {

	/**
		<summary>
		Approximate equivalent of JavaScript Array, intended to smooth transitions between
		C++ and C#/JS versions.
		</summary>
		*/

	template <typename T>
	class Array {
		friend class KmerDistanceCalculator;
	protected:
		size_t length;
		T * data;

	public:
		/// <summary>
		/// If the constructor is called with a single numeric argument, it 
		/// creates a new typed array with the specified number of elements, 
		/// and initializes each element to the default (if any) for T.
		/// </summary>
		/// <param name="length"></param>
		Array(size_t length = 0) : length(length) {
			// this->length = length;
			data = new T[length];
		}

		/// <summary>
		/// If passed a single typed array object, the constructor creates a 
		/// new typed array with the same number of arguments as the argument 
		/// array, and then copies the elements of the argument array to the 
		/// newly created array. The type of the argument array need not be the 
		/// same as the type of the array being created.
		/// </summary>
		/// <param name="array"></param>
		Array(const Array & array) : length(array.length) {
			//length = other.Length();
			data = new T[length];

			for ( size_t i = 0; i < length; i++ ) {
				data[i] = array.data[i];
			}
		}

		/// <summary>
		/// If passed a single array (a true JavaScript array), the constructor 
		/// creates a new typed array with the same number of arguments and 
		/// then copies element values from the argument array into the new 
		/// array.
		/// </summary>
		/// <param name="array"></param>
		Array(size_t length, T array[]) : length(length) {
			// length = other.Length();
			data = new T[length];

			for ( size_t i = 0; i < length; i++ ) {
				data[i] = array[i];
			}
		}

		/// <summary>
		/// If passed a single array (a true JavaScript array), the constructor 
		/// creates a new typed array with the same number of arguments and 
		/// then copies element values from the argument array into the new 
		/// array.
		/// </summary>
		/// <param name="array"></param>
		Array(const vector<T> & vec) {
			length = vec.size();
			data = new T[length];

			for ( size_t i = 0; i < length; i++ ) {
				data[i] = vec.data[i];
			}
		}

		virtual ~Array() {
			delete[] data;
		}

		void operator = (const Array & other) {
			Resize(other.length);

			for ( size_t i = 0; i < length; i++ ) {
				data[i] = other.data[i];
			}
		}

		/// <summary>
		///		The number of elements in the array. TypedArrays have fixed size, 
		///		and the value of this property never changes. Note that this property 
		///		is not, in general, the same as the byteLength property inherited from 
		///		ArrayBufferView.
		/// </summary>
		size_t Length() {
			return length;
		}

		void Resize(size_t newLength) {
			if ( length != newLength ) {
				delete[] data;
				length = newLength;
				data = new T[length];
			}
		}

		T & operator[] (size_t index) const {
			if ( index >= length ) {
				throw Exception("Index out of bounds", __FILE__, __LINE__);
			}
			return data[index];
		}

		/// <summary>
		/// Copy elements of array into this typed array, starting at index 
		/// offset.
		/// </summary>
		/// <param name="array"></param>

		void Set(Array & array) {
			Set(array.length, array.data, 0);
		}

		/// <summary>
		/// Copy elements of array into this typed array, starting at index 
		/// offset.
		/// </summary>
		/// <param name="array"></param>
		/// <param name="offset"></param>

		void Set(Array & array, long offset) {
			Set(array.length, array.data, offset);
		}

		/// <summary>
		/// Copy elements of JavaScript array into this typed array, starting at
		/// index offset.
		/// </summary>
		/// <param name="array"></param>

		void Set(size_t length, T array[]) {
			Set(length, array.data, 0);
		}

		/// <summary>
		/// Copy elements of regular array into this typed array, starting at
		/// index offset.
		/// </summary>
		/// <param name="array"></param>
		/// <param name="offset"></param>

		void Set(size_t length, T array[], long offset) {
			if ( length + offset > this.length ) throw Exception("Index out of bounds.");

			for ( size_t i = 0; i < length; i++ ) {
				data[i + offset] = array[i];
			}
		}

		/**
		 * <summary>
		 *	Files the srray with the specified value.
		 * </summary>
		 */

		void Fill(T value) {
			for ( size_t i = 0; i < length; i++ ) {
				data[i] = value;
			}
		}

		void Sort() {
			sort(data, data + length);
		}

		void Sort(Func2<T &, T &, bool> comparison) {
			sort(data, data + length, comparison);
		}

		T * Data() {
			return data;
		}

	};

	template <typename T>
	struct RawVector {
		T * data;
		size_t size;

		RawVector(size_t size) : size(size), data(new T[size]) {}

		virtual ~RawVector() {
			delete[] data;
		}
	};

	template <typename T>
	struct RawMatrix {
		T ** data;
		size_t rows, cols;

		RawMatrix(size_t rows, size_t cols) : rows(rows), cols(cols), data((T **) malloc(rows * sizeof(T *))) {
			for ( size_t r = 0; r < rows; r++ ) {
				data[r] = (double *) malloc(cols * sizeof(T));
			}
		}

		virtual ~RawMatrix() {
			for ( size_t r = 0; r < rows; r++ ) {
				delete[] data[r];
				data[r] = 0;
			}

			delete[] data;
		}
	};

	template <typename T>
	class FlatMatrix {
		T * data;
		size_t rows_, cols_;

		friend class KmerDistanceCache;
	public:
		FlatMatrix(size_t rows = 0, size_t cols = 0) : rows_(rows), cols_(cols), data(new T[rows * cols]) {
			if ( !data ) {
				throw Exception("Out of memory!", FileAndLine);
			}
		}

		FlatMatrix(size_t rows, size_t cols, T value) : rows_(rows), cols_(cols), data(new T[rows * cols]) {
			if ( !data ) {
				throw Exception("Out of memory!", FileAndLine);
			}

			fill(value);
		}

		FlatMatrix(FlatMatrix & other) :
			rows_(other.rows_),
			cols_(other.cols_),
			data(new T[other.rows_ * other.cols_]) {

			if ( !data ) {
				throw Exception("Out of memory!", FileAndLine);
			}

			for ( size_t i = 0; i < rows_ * cols_; i++ ) {
				data[i] = other.data[i];
			}
		}

		FlatMatrix & operator=(FlatMatrix & other) {
			if ( this == &other ) return *this;

			if ( rows_ * cols_ != other.rows_ * other.cols_ ) {
				delete[] data;
				rows_ = other.rows_;
				cols_ = other.cols_;
				data = new T[rows_ * cols_];
				if ( !data ) {
					throw Exception("Out of memory!", FileAndLine);
				}
			}

			memcpy(data, other.data, rows_ * cols_ * sizeof(T));

			return *this;
		}

		virtual ~FlatMatrix() {
			delete[] data;
		}

		void resize(size_t newRows, size_t newCols) {
			if ( rows_ * cols_ != newRows * newCols ) {
				delete[] data;
				rows_ = newRows;
				cols_ = newCols;
				data = new T[rows_ * cols_];
				if ( !data ) {
					throw Exception("Out of memory!", FileAndLine);
				}
			}
		}

		void fill(const T & value) {
			for ( int i = 0; i < rows_ * cols_; i++ ) {
				data[i] = value;
			}
		}

		/*
		**	Sets the contents of the matrix to 0 using memset().
		*/

		void clear( T defaultValue ) {
			for ( int i = 0; i < rows_ * cols_; i++ ) {
				data[i] = defaultValue;
			}
		}

		/*
		**	Gets a reference to the matrix element element at
		**	position (r,c), where 0 <= r < rows and 0 <= c < cols.
		*/

		T & operator()(size_t r, size_t c) {
			//if ( r >= rows_ || c >= cols_ ) {
			//	throw Exception( "Index out of bounds.", FileAndLine );
			//}
			return data[r * cols_ + c];
		}

		/*
		** Serialises the object to a stream.
		*/

		friend ostream & operator << (ostream & out, const FlatMatrix<T> & matrix) {
			T *ptr = matrix.data;
			for ( int i = 0; i < matrix.rows_; i++ ) {
				for ( int j = 0; j < matrix.cols_; j++ ) {
					if ( j > 0 ) out << ',';
					out << *ptr++;
				}
				out << endl;
			}

			return out;
		}

		size_t rows() const { return rows_; }

		size_t cols() const { return cols_; }

		T * buffer() const { return data; }

		T * row(size_t r) const {
			return data + r * cols_;
		}

		/*
		** Adds another matrix to this one, element by element.
		*/
		void operator += (const FlatMatrix & other) {
			for ( int i = 0; i < rows_ * cols_; i++ ) {
				data[i] += other.data[i];
			}
		}

		/*
		** Subtracts another matrix from this one, element by element.
		*/
		void operator -= (const FlatMatrix & other) {
			for ( int i = 0; i < rows_ * cols_; i++ ) {
				data[i] -= other.data[i];
			}
		}

		/*
		** Multiplies the matrix by another, element by element.
		*/
		void operator *= (const FlatMatrix & other) {
			for ( int i = 0; i < rows_ * cols_; i++ ) {
				data[i] *= other.data[i];
			}
		}

		/*
		** Divides the matrix by another, element by element.
		*/
		void operator /= (const FlatMatrix & other) {
			for ( int i = 0; i < rows_ * cols_; i++ ) {
				data[i] /= other.data[i];
			}
		}

		/*
		** Multiplies the matrix by another, element by element.
		*/
		void operator *= (T scalar) {
			for ( int i = 0; i < rows_ * cols_; i++ ) {
				data[i] *= scalar;
			}
		}

		/*
		** Divides the matrix by another, element by element.
		*/
		void operator /= (T scalar) {
			for ( int i = 0; i < rows_ * cols_; i++ ) {
				data[i] /= scalar;
			}
		}

		/**
		**  This is an equivalence relation. Complexity is linear in the size of the
		**  matrices. Matrices are considered equivalent if their dimensions are equal,
		**  and if corresponding elements compare equal.
		*/
		bool operator==(const FlatMatrix<T> & y) const {
			return (rows_ == y.rows_
				&& cols_ == y.cols_
				&& memcmp(data, y.data, rows_ * cols_ * sizeof(T)) == 0
				);
		}

		/**
		**  This is an equivalence relation. Complexity is linear in the size of the
		**  matrices. Matrices are considered equivalent if their dimensions are equal,
		**  and if corresponding elements compare equal.
		*/
		bool operator!=(const FlatMatrix<T> & y) const {
			return (rows_ != y.rows_
				|| cols_ != y.cols_
				|| memcmp(data, y.data, rows_ * cols_ * sizeof(T)) != 0
				);
		}

		// TODO: other linear algebra, as required and makes sense.
	};

	template<typename T>
	class subvector {
	public:
		vector<T> * base;
		size_t offset;
		size_t length;

		subvector() : base(0), length(0), offset(0) {}

		subvector(vector<T> * base, size_t offset, size_t length)
			: base(base), offset(offset), length(length) {
			if ( offset + length > base->size() ) throw Exception("Bad offset and length in subvector", FileAndLine);
		}

		subvector(const subvector * other) : base(other.base), offset(other.offset), length(other.length) {}

		subvector & operator=(const subvector * other) { base = other.base; offset = other.offset; length = other.length; }

		T & operator[](size_t i) {
#if defined(SUBVECTOR_BOUNDS)
			if ( i >= length ) throw Exception("bad index in subvector", FileAndLine );
#endif
			return (*base)[offset + i];
		}

		T * data() {
			return base->data() + offset;
		}

		size_t size() { return length; }
	};
}
