#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <vector>
#include <string>

using namespace std;

namespace QutBio{

	class IArrayParser {
	public: virtual void Parse( vector<string> & fields ) = 0;
	};

}