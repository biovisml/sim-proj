#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include "FragmentAggregationMode.hpp"
#include "HausdorffCalculator.hpp"

namespace QutBio {
	/**
	 *	<summary>
	 *		A subclass of HausdorffCalculator that reveals the mutual distance matrix and fragment distance cache
	 *		for use by programs that need efficient access to an exhaustive MxN distance matrix.
	 */
	class OpenHausdorff :
		public HausdorffCalculator {
	public:
		OpenHausdorff(
			SimilarityMatrix * matrix,
			uint kmerLength,
			FragmentAggregationMode * kmerMode,
			FragmentAggregationMode * fragMode,
			Alphabet * alphabet,
			uint fragLength,
			uint maxQueryLength,
			uint maxSubjectLength,
			bool pushKmerDistances
			) : HausdorffCalculator(
			matrix, kmerLength, kmerMode, fragMode, alphabet, fragLength, maxQueryLength, maxSubjectLength, pushKmerDistances
			) {}

		Distance ** Distances() { return kmerDistCache; }

		vector<Distance> & RowMinima() { return rowMinima; }
		vector<Distance> & ColMinima() { return colMinima; }
	};

}
