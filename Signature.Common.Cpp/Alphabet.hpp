#pragma once

#include <string>
using namespace std;

#include "EncodedKmer.hpp"
#include "EnumBase.hpp"
#include "SimilarityMatrix.hpp"
#include "Array.hpp"

using namespace QutBio;

namespace QutBio {

	class Alphabet : public EnumBase {
	private:
		string symbols;
		unsigned char inverse[128];

		Alphabet( string literal, int value )
			: EnumBase( literal, value ) {
			symbols = value == 0 ? SimilarityMatrix::Blosum62()->Symbols() : string("acgt");

			for ( size_t i = 0; i < symbols.size(); i++ ) {
				inverse[tolower(symbols[i])] = (unsigned char) i;
				inverse[toupper(symbols[i])] = (unsigned char) i;
			}
		}

	public:
		Alphabet(SimilarityMatrix * matrix): EnumBase( "Custom", 2 ) {
			symbols = matrix->Symbols();

			for ( size_t i = 0; i < symbols.size(); i++ ) {
				inverse[tolower(symbols[i])] = (unsigned char) i;
				inverse[toupper(symbols[i])] = (unsigned char) i;
			}	
		}

		static Alphabet * AA() {
			static Alphabet value( "AA", 0 );
			return &value;
		}

		static Alphabet * DNA() {
			static Alphabet value( "DNA", 1 );
			return &value;
		}

		static Array<EnumBase *> & Values() {
			static Array<EnumBase *> result( 2 );
			result[0] = AA();
			result[1] = DNA();
			return result;
		}

		static size_t WordsPerKmer( size_t kmerLength, size_t charsPerWord ) {
			return ( kmerLength + charsPerWord - 1 ) / charsPerWord;
		}

		/// <summary> Encodes a single kmer as a sequence of zero-origin numeric values.
		/// </summary>
		/// <param name="s"></param>
		/// <param name="charsPerWord">The number of encoded characters to pack into each word.
		///		This is independent of any particular kmer tiling scheme. It has to do with the
		///		maximum size of a pre-computed kmer similarity lookup table that can be created.
		///		<para>
		///			For my current experiments, I use charPerWord == 3 for amino acid, with the
		///			BLOSUM alphabet, this yields tables of approx 1/4GB.
		///		</para>
		/// </param>
		/// <returns></returns>

		void Encode( const char * s, size_t kmerLength, size_t charsPerWord, EncodedKmer code ) {
			size_t words = WordsPerKmer( kmerLength, charsPerWord );
			size_t size = symbols.size();
			size_t wordIndex = 0;
			code[wordIndex] = 0;

			for ( size_t i = 0; i < kmerLength; i++ ) {
				int j = inverse[s[i]];
				code[wordIndex] = code[wordIndex] * (int) size + j;

				if ( i > 0 && i % charsPerWord == ( charsPerWord - 1 ) ) {
					wordIndex++;

					if ( wordIndex < words ) {
						code[wordIndex] = 0;
					}
				}
			}
		}

		/// <summary> Encodes the designated string by computing a codeword for each tuple of 
		///		charsPerWord contiguous symbols. Multiple lists of codewords are calculated,
		///		one for each offset in 0, ..., (charsPerWord-1), and each of these lists becomes 
		///		a row of the resulting output dataset, code. 
		///		
		///		After processing:
		///		*	code[offset] contains the codewords of tuples starting at positions
		///			i*charsPerWord + offset for i = 1..(len / charsPerWord).
		/// </summary>
		/// <param name="s"></param>
		/// <param name="charsPerWord">The number of encoded characters to pack into each word.
		///		This is independent of any particular kmer tiling scheme. It has to do with the
		///		maximum size of a pre-computed kmer similarity lookup table that can be created.
		///		<para>
		///			For my current experiments, I use charsPerWord \in {2,3} for amino acid, with the
		///			BLOSUM alphabet, this yields in-memory distance tables of approx 1/4GB.
		///		</para>
		/// </param>
		/// <returns></returns>

		void Encode( const char * s, size_t len, size_t kmerLength, size_t charsPerWord, vector<vector<KmerWord>> & code ) {
			if ( kmerLength % charsPerWord != 0 ) {
				stringstream str;
				str << "Alphabet::Encode: kmerLength must be divisible by charsPerWord: kmerLength = "
					<< kmerLength
					<< ", charsPerWord = "
					<< charsPerWord
					<< "\n";
				throw Exception(  str.str(), FileAndLine );
			}

			code.resize(charsPerWord);
			
			for ( size_t i = 0; i < charsPerWord; i++ ) {
				code[i].clear();
				code[i].reserve(len/charsPerWord);
			}

			for ( size_t i = 0; i < len - charsPerWord + 1; i++ ) {
				KmerWord codeWord;
				Encode( s + i, charsPerWord, charsPerWord, &codeWord );
				code[i % charsPerWord].push_back( codeWord );
			}
		}

		/// <summary> Decodes a sequence of zero-origin numeric values into a string.
		/// </summary>
		/// <param name="code">The sequence of numeric code values.</param>
		/// <param name="k">The number of symbols required in the decoded string.</param>
		/// <param name="charsPerWord">The number of characters packed into each 
		///		word. See Encode for details.
		///	</param>
		/// <returns></returns>

		void Decode( const KmerWord * code, size_t k, size_t charsPerWord, char * charBuffer ) {
			size_t words = ( k + charsPerWord - 1 ) / charsPerWord;
			size_t size = symbols.size();
			string * s = new string();
			vector<string *> t = {};
			t.push_back( s );
			size_t wordIndex = 0;
			long n = code[wordIndex];

			for ( int i = 0; i < k; i++ ) {
				int j = n % size;
				n = ( n - j ) / (int) size;
				s->insert(0, 1, symbols[j] );

				if ( i > 0 && i % charsPerWord == ( charsPerWord - 1 ) ) {
					wordIndex++;

					if ( wordIndex < words ) {
						s = new string();
						t.push_back( s );
						n = code[wordIndex];
					}
				}
			}

			int buffPos = 0;

			for ( size_t i = 0, iMax = t.size(); i < iMax; i++ ) {
				for ( size_t j = 0, jMax = t[i]->size(); j < jMax; j++ ) {
					charBuffer[buffPos++] = (*t[i])[j];
				}
			}
		}

		/// <summary> Decodes a sequence of zero-origin numeric values into a string.
		/// </summary>
		/// <param name="code">The sequence of numeric code values.</param>
		/// <param name="len">The number of symbols required in the decoded string.</param>
		/// <param name="charsPerWord">The number of characters packed into each 
		///		word. See Pack for details.
		///	</param>
		/// <returns></returns>

		void Decode( const vector<vector<KmerWord>> & code, size_t len, size_t charsPerWord, char * charBuffer ) {
			for ( size_t i = 0; i < len - charsPerWord + 1; i++ ) {
				Decode( & code[i%charsPerWord][i/charsPerWord], charsPerWord, charsPerWord, charBuffer + i );
			}

			charBuffer[len] = 0;
		}

		/// <summary> Gets the number of symbols in this alphabet.
		/// </summary>

		int Size() { 
			return (int) symbols.size(); 
		}

		/// <summary> Gets the symbols defined in this alphabet.
		/// </summary>

		string Symbols() {
				return symbols;
			}

	};

}
