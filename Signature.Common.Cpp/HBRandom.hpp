#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <random>

namespace QutBio {
	class UniformRealRandom {
		std::default_random_engine generator;
		std::uniform_real_distribution<double> distribution;
	public:
		UniformRealRandom(unsigned long seed) : distribution(0.0, 1.0) {
			generator.seed(seed);
		}

		double operator()() {
			return distribution(generator);
		}
	};

	template<typename T = int>
	class UniformIntRandom {
		std::default_random_engine generator;
		std::uniform_int_distribution<T> distribution;
	public:
		UniformIntRandom(unsigned long seed, T min, T max) : distribution( min, max ) {
			generator.seed(seed);
		}

		T operator()() {
			auto x = distribution(generator);
			return x;
		}
	};
}
