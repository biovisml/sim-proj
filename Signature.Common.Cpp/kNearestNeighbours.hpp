#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <vector>

namespace QutBio {
	/**
	*  <p>
	*      Uses a fixed-size min heap to implement a k-nearest neighbour
	*      accumulator.
	*  </p>
	*  <p>
	*      The data is stored in approximately ascending order (the heap
	*      property) with the gratest value at the back of the list. (Usually,
	*      a min heap would have the smallest value at the front but the
	*      greatest value could be someplace near, but not necessarily
	*      at, the back.)
	*  </p>
	*/
	template<class T, typename Compare = less<T>>
	class KNearestNeighbours {
	protected:
		vector<T> heap;
		const Compare & compare;
		size_t capacity;
	public:
		KNearestNeighbours(size_t capacity = 0, const Compare & compare = Compare()) : compare(compare), capacity(capacity) {
			heap.reserve(capacity);
		}

		void setCapacity(size_t capacity) {
			this->capacity = capacity;
			heap.reserve(capacity);
		}

		size_t getCapacity() {
			return capacity;
		}

		void clear() {
			heap.clear();
		}

		void push(const T & item) {
			size_t last = heap.size() - 1;

			if ( last + 1 == capacity ) {
				if ( compare(item, heap[last] ) ) {
					heap[last] = item;
				}
			}
			else {
				heap.push_back(item);
			}

			make_heap(heap.rbegin(), heap.rend(), compare);
		}

		template<typename Collection>
		void push(const Collection & items) {
			for ( T item : items ) {
				push(item);
			}
		}

		const T & top() const {
			return heap.back();
		}

		void pop() {
			heap.pop_back();
			make_heap(heap.rbegin(), heap.rend(), compare);
		}

		bool empty() {
			return heap.empty();
		}

		typedef typename std::vector<T>::iterator iterator;

		iterator begin() {
			auto iter = heap.begin();
			return iter;
		}

		iterator end() {
			auto iter = heap.end();
			return iter;
		}

		friend ostream & operator << (ostream & out, KNearestNeighbours<T, Compare> & knn) {
			bool deja = false;

			for ( auto x : knn ) {
				if ( deja ) cout << ", "; else deja = true;
				out << x;
			}

			out << endl;
			return out;
		}
	};
}
