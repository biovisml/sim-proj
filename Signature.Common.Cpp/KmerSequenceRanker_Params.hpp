#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <string>
#include "FragmentAggregationMode.hpp"
#include "DistanceType.hpp"
#include "Alphabet.hpp"

using namespace std;

namespace QutBio {

	struct KmerSequenceRanker_Params {
		string queryFile;
		string dbFile;
		string codebookFile;
		string matrixFile;

		string rankingFile;

		int idIndex = 0;
		int classIndex = -1;
		int idDigits = 5;
		int kmerLength = 30;
		int fragLength = 1;
		FragmentAggregationMode * fragMode = FragmentAggregationMode::HausdorffAverage();
		FragmentAggregationMode * kmerMode = FragmentAggregationMode::HausdorffAverage();
		DistanceType * dist = DistanceType::Blosum();
		int matrixId = 62;
		SimilarityMatrix * matrix;
		Alphabet * alphabet = Alphabet::AA();

		Distance thresholdDistance = BAD_DIST;
		Distance defaultDistance = BAD_DIST;

		bool isCaseSensitive = false;

		bool pushKmerDistances = false;

		int maxRecords = 1000;

		int sampleSize = 1000;

		int skip = 1;

		int seed = (int) time(NULL);

		string queryIdFile = "";

		/// <summary> Parses and validates the arguments, returning the results in a Params object.
		/// </summary>
		/// <param name="arguments"></param>
		/// <returns></returns>

		KmerSequenceRanker_Params(Args & arguments) {
			if ( !arguments.Get("dbFile", dbFile) ) {
				cerr << "Argument 'dbFile' not defined." << endl;
				ShowHelp(); exit(1);
			}

			if ( !arguments.Get("queryFile", queryFile) ) {
				cerr << "Argument 'queryFile' not defined." << endl;
				ShowHelp(); exit(1);
			}

			if ( arguments.IsDefined("idDigits") ) {
				arguments.Get("idDigits", idDigits );
			}

			if ( !(idDigits >= 0) ) {
				cerr << "Argument 'idDigits' not valid." << endl;
				ShowHelp(); exit(1);
			}

			if ( arguments.IsDefined("idIndex") ) {
				arguments.Get("idIndex", idIndex);
			}

			if ( !(idIndex >= 0) ) {
				cerr << "Argument 'idIndex' not valid." << endl;
				ShowHelp(); exit(1);
			}

			if ( arguments.IsDefined("classIndex") ) {
				arguments.Get("classIndex", classIndex);
			};

			if ( !(classIndex != idIndex) ) {
				cerr << "Argument 'classIndex' must be different from 'idIndex'." << endl;
				ShowHelp(); exit(1);
			}

			if ( !(arguments.Get("fragLength", fragLength) && fragLength > 0) ) {
				cerr << "Argument 'fragLength' not valid." << endl;
				ShowHelp(); exit(1);
			}

			if ( !(arguments.Get("fragMode", FragmentAggregationMode::Values(), fragMode) && fragMode != 0) ) {
				cerr << "Argument 'fragMode' not valid." << endl;
				ShowHelp(); exit(1);
			}

			if ( !(arguments.Get("kmerMode", FragmentAggregationMode::Values(), kmerMode) && kmerMode != 0) ) {
				cerr << "Argument 'kmerMode' not valid." << endl;
				ShowHelp(); exit(1);
			}

			if ( !(arguments.Get("kmerLength", kmerLength)) ) {
				cerr << "Argument 'kmerLength' not valid." << endl;
				ShowHelp(); exit(1);
			}

			if ( !(kmerLength > 0) ) {
				cerr << "Argument 'kmerLength' not valid." << endl;
				ShowHelp(); exit(1);
			}
			// assert_true( kmerLength <= 9 );

			if ( arguments.IsDefined("alphabet") ) {
				if ( !arguments.Get("alphabet", Alphabet::Values(), alphabet) ) {
					cerr << "Unable to parse argument 'alphabet'." << endl;
					ShowHelp(); exit(1);
				}
			}
			else {
				alphabet = Alphabet::AA();
			}

			if ( !(arguments.Get("dist", DistanceType::Values(), dist)) ) {
				cerr << "Argument 'dist' not valid." << endl;
				ShowHelp(); exit(1);
			}

			if ( arguments.IsDefined("matrixId") ) {
				if ( !(arguments.Get("matrixId", matrixId)) ) {
					cerr << "Argument 'matrixId' not valid." << endl;
					ShowHelp(); exit(1);
				}

				vector<int> matrices{35, 40, 45, 50, 62, 80, 100};

				bool found = false;

				for ( auto x : matrices ) {
					if ( x == matrixId ) { found = true; }
				}

				if ( !found ) {
					cerr << "Matrix id not recognised." << endl;
					ShowHelp(); exit(1);
				}
			}

			bool mustReplaceAlphabet = false;

			if ( arguments.IsDefined("matrixFile") ) {
				arguments.Get("matrixFile", matrixFile);
				mustReplaceAlphabet = true;
				dist = DistanceType::Custom();
				matrixId = -1;
			}

			arguments.GetOptionalArgument("isCaseSensitive", isCaseSensitive, ShowHelp );

			matrix = SimilarityMatrix::GetMatrix( dist, matrixId, matrixFile, isCaseSensitive );

			if ( mustReplaceAlphabet ) {
				alphabet = new Alphabet( matrix );
			}

			if ( !arguments.Get("rankingFile", rankingFile) ) {
				cerr << "Argument 'rankingFile' not defined." << endl;
				ShowHelp(); exit(1);
			}

			if ( !arguments.Get("codebookFile", codebookFile) ) {
				cerr << "Argument 'codebookFile' not defined.\nCondition treating as warning.\nRelevance must be determined manually." << endl;
			}

#define get_opt_arg(v) arguments.GetOptionalArgument( #v, v, ShowHelp )

			get_opt_arg(thresholdDistance);
			get_opt_arg(defaultDistance);
			get_opt_arg(pushKmerDistances);
			get_opt_arg(maxRecords);
			get_opt_arg(sampleSize);
			get_opt_arg(skip);
			get_opt_arg(seed);
			get_opt_arg(queryIdFile);

			if ( skip <= 0 ) {
				cerr << "Argument 'skip' must be greater than zero." << endl;
			}

			if ( seed == -1 ) {
				seed = (int) time(NULL);
			}
		}

		/// <summary> Displays general help.
		/// </summary>

		static void ShowHelp() {
			string helpText =
				"KmerRank:"  "\n"
				"--------"  "\n"
				"Computes the document ranking of a query dataset under the two - level hierarchical kmer similarity measure."  "\n"
				""  "\n"
				"Required arguments :"  "\n"
				"--------------------"  "\n"
				"--dbFile (fileName) -- The name of a FASTA formatted file containing the database to be queried."  "\n"
				"--queryFile (fileName) -- The name of a FASTA-formatted file containing query sequences. If this"  "\n"
				"    is the same as dbFile, the queries will be sampled uniformly without replacement from the "  "\n"
				"    database." "\n"
				"--kmerLength (uint) -- The number of characters in the kmer tiling."  "\n"
				"--dist (UngappedEdit|Blosum|Custom) -- The distance measure to use. Custom indicates that a "  "\n"
				"    custom substitution matrix will be supplied."  "\n"
				"--fragLength (uint) -- The fragment length used to partition sequences."  "\n"
				"--fragMode (BestOfBest|Hausdorff|HausdorffAverage) -- The combination mode used to aggregate " "\n" 
				"    fragment distances to produce an overall sequence distance."  "\n"
				"--kmerMode (BestOfBest|Hausdorff|HausdorffAverage) -- The combination mode used to aggregate "  "\n"
				"    kmer distances to produce a fragment distance." "\n"
				"--rankingFile (fileName) -- The name of a file which will be overwritten with the CSV-formatted " "\n" 
				"    ranking results."  "\n"
				"\n"
				"Optional arguments :"  "\n"
				"--------------------"  "\n"
				"--queryIdFile (fileName) -- Name of a file which, if supplied, will be overwritten with a list of " "\n"
				"    query sequence IDs. This is useful if the queries were sampled from the database, and can be " "\n" 
				"    used to extract a suitable subset of the qrels file for accurate Precision-Recall calculations." "\n"
				"--matrixId (35|40|45|50|62|80|100 ) -- The blosum matrix to use, if --dist Blosum is " "\n" 
				"    set. Default value is 62."  "\n"
				"--idIndex (int) -- The zero-origin index position of the ID field in the pipe-separated FASTA " "\n" 
				"    definition line. Default value is 0. Each sequence must have a unique Id which will be used"  "\n"
				"    in the output ranking file." "\n"
				"--classIndex (int) -- The zero-origin index position of the class label field in the pipe-separated " "\n" 
				"    FASTA definition line. Default value is 1. If no field suits this purpose, use -1."  "\n"
				"    Note that the class label (even if present) is not used in any way by this program."  "\n"
				"--alphabet (AA|DNA|Custom)	-- The alphabet from which symbols are assumed to be drawn. " "\n" 
				"    Default is Amino Acid(AA). Use Custom when you want to infer the alphabet from a custom"  "\n"
				"    similarity matrix." "\n"
				"--thresholdDistance (double > 0) -- The threshold distance used to select clusters from the codebook." "\n"
				"--pushKmerDistances (true|false) -- An arcane setting related to some of the exhaustive search algorithms" "\n"
				"    Should I use the \"feed-forward\" mechanism to propagate kmer distances for Hausdorff calculators? " "\n" 
				"    Feed-forward is slower if an exhaustive kmer distance table is processed, but it is better able to" "\n" 
				"    deal with the situation where the database is scanned kmer-by-kmer rather than sequence by sequence." "\n"
				"--skip (int > 0) -- The sampling rate for the query sequence. Skip = 1 samples every kmer, while " "\n" 
				"    skip == 2 samples every second kmer, etc. Default value: 1." "\n"
				"--seed (int) -- Seed for random number generator. " "\n" 
				"    Default value (used if seed is missing or negative): time(NULL)." "\n"
				"--maxRecords: (int) -- Optional number of rankings to emit. Default value: 1000." "\n"
				"--sampleSize: (int) -- Optional number of observations to be selected for test. Default value: 1000. " "\n"
				"    Set this to 0 to process the entire query dataset." "\n"
				;

			cout << helpText;
		}
	};
}
