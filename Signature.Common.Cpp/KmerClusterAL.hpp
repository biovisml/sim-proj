#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <vector>
#include <iterator>
#include <cstdio>

#include "Kmer.hpp"
#include "SimilarityMatrix.hpp"
#include "KmerDistanceCache.hpp"

namespace QutBio {

	/**
	 *  <summary>
	 *	Pairwise symbol distance function mapping 0..(aSize-1) \times 0..(aSize-1) \to DistanceType.
	 *	I use KmerWord as the argument type, which may in some applications may contain 2 or 3
	 *	packed symbols, but in this context, it should contain a single encoded symbol value
	 *	between 0 and (aSize-1).
	 *	</summary>
	 */
	typedef function<Distance(KmerWord, KmerWord)> SymbolDistanceFunction;

	/**
	 *  <summary>
	 *	An Average-linkage Kmer Cluster.
	 *
	 *  The distance of a kmer to the cluster as a whole is the average of the distances from the
	 *  kmer to the individual kmers assigned to the cluster.
	 *
	 *  A position-weight matrix is used to optimise the average calculation (maximum benefit
	 *  is attained for large clusters).
	 *	</summary>
	 */
	struct KmerClusterAL {
		// The length of kmers.
		size_t K;

		/**
		 *	<summary>
		 *	The size of alphabet.
		 *	</summary>
		 */
		size_t alphaSize;

		/**
		 *	<summary>
		 *	Function used to determine symbol distances.
		 *	</summary>
		 */
		SymbolDistanceFunction symbolCodeDist;

		/**
		 *	<summary>
		 *	The kmers associated with this cluster.
		 *	</summary>
		 */
		vector<Kmer> kmers;

		/**
		 *	<summary>
		 * A k \times aSize matrix containing the count of each residue
		 * at each of the k positions in the kmer.
		 *
		 * Invariant:
		 * ----------
		 *	Let m = kmers.size().
		 *	For i in 0..(k-1):
		 *		For a in 0..(aSize-1):
		 *			pwm[i][a] == count (j=0..(m-1)) kmers[j][i] == a.
		 *		End
		 *	End
		 *	</summary>
		 */
		FlatMatrix<uint> pwm;

		/**
		 *	<summary>
		 *	Initialise a new, empty, Average-linkage cluster.
		 *	</summary>
		 */
		KmerClusterAL(size_t K, size_t alphaSize, SymbolDistanceFunction symbolCodeDist)
			: K(K), alphaSize(alphaSize), symbolCodeDist(symbolCodeDist) {
			pwm.resize(K, alphaSize);
			pwm.clear(0);
		}

		virtual ~KmerClusterAL() {}

		double DistanceTo(FastaSequence * seq, size_t offset) {
			if ( kmers.size() == 0 ) {
				return NAN;
			}

			Kmer kmer(seq, offset, K);
			EncodedKmer code = kmer.Word();
			return DistanceTo(code);
		}

		double DistanceTo(const Kmer & kmer) {
			return DistanceTo(kmer.Word());
		}

		double DistanceTo(KmerWord code[]) {
			double sum = 0;

			for ( uint k = 0; k < K; k++ ) {
				auto weights = pwm.row(k);

				for ( KmerWord j = 0; j < alphaSize; j++ ) {
					if ( weights[j] > 0 ) {
						auto d = symbolCodeDist(j, code[k]);
						sum += d * weights[j];
					}
				}
			}

			sum /= kmers.size();
			return sum;
		}

		double DistanceTo(const KmerClusterAL & other) {
			double sum = 0;

			for ( uint k = 0; k < K; k++ ) {
				auto w1 = pwm.row(k);
				auto w2 = other.pwm.row(k);

				for ( KmerWord i = 0; i < alphaSize; i++ ) {
					for ( KmerWord j = 0; j < alphaSize; j++ ) {
						auto d = symbolCodeDist(i, j);
						sum += d * w1[i] * w2[j];
					}
				}
			}

			sum /= (kmers.size() * other.kmers.size());
			return sum;
		}

		/*
		**	Adds a kmer to the cluster, updating the kmer collection
		**	and the position weight matrix.
		*/

		void Add(FastaSequence * seq, size_t offset) {
			kmers.emplace_back(seq, offset, K);
			EncodedKmer code = kmers[kmers.size() - 1].Word();

			for ( uint k = 0; k < K; k++ ) {
				pwm(k, code[k])++;
			}
		}

		/*
		**	Adds a kmer to the cluster, updating the kmer collection
		**	and the position weight matrix.
		*/

		void Add(const Kmer & kmer) {
			Add(kmer.sequence, kmer.kmerPosition);
		}

		/*
		**	Adds all kmers from another cluster to this cluster. Equivalent
		**	to calling Add(const Kmer &) once for each kmer in the other
		**	cluster and the position weight matrix.
		*/

		void Add(const KmerClusterAL & other) {
			for ( auto & kmer : other.kmers ) {
				Add(kmer);
			}
		}

		/*
		**	Merges another cluster with this cluster. Equivalent
		**	to calling Add(const Kmer &) once for each kmer in the other
		**	cluster and the position weight matrix, but should be somewhat
		**	faster
		*/

		void Merge(const KmerClusterAL & other) {
			kmers.insert(kmers.end(), other.kmers.begin(), other.kmers.end());
			pwm += other.pwm;
		}

		/*
		**	Removes all kmers from the cluster.
		*/

		void Clear() {
			kmers.clear();
			pwm.clear(0);
		}

		/*
		**	Gets the average distance from a kmer in the cluster to the
		**	centroid.
		*/

		double AverageDistortion() {
			if ( kmers.size() < 2 ) {
				return NAN;
			}

			double totalDistortion = 0;

			for ( auto & kmer : kmers ) {
				totalDistortion += DistanceTo(kmer);
			}

			return totalDistortion / kmers.size();
		}

		using Prototype = pair<Kmer*, double>;

		/*
		**	Gets the (first encountered) stored kmer with the lowest quantization error.
		*/

		Prototype GetCentralKmer(void) {
			Prototype result;

			if ( kmers.size() < 1 ) {
				result.first = 0;
				result.second = NAN;
			}
			else {
				result.second = numeric_limits<double>::max();

				for ( auto & kmer : kmers ) {
					auto dist = DistanceTo(kmer);

					if ( dist < result.second ) {
						result.first = &kmer;
						result.second = dist;
					}
				}
			}

			return result;
		}

		/*
		**	Gets the average distance from a kmer in the cluster to the
		**	centroid.
		*/

		double AverageDistortionParallel(int numThreads) {
			if ( kmers.size() < 2 ) {
				return NAN;
			}

			vector<double> totalDistortion(numThreads);
			const int N = (int) kmers.size();

#pragma omp parallel for
			for ( int i = 0; i < N; i++ ) {
				int threadNum = omp_get_thread_num();
				totalDistortion[threadNum] += DistanceTo(kmers[i]);
			}

			return sum(totalDistortion.begin(), totalDistortion.end(), 0.0) / kmers.size();
		}

		/*
		**	Gets the (first encountered) stored kmer with the lowest quantization error.
		**	Uses explicit OpenMP parallel for.
		*/

		Prototype GetCentralKmerParallel(int numThreads) {
			if ( kmers.size() < 1 ) {
				Prototype result;
				result.first = 0;
				result.second = NAN;
				return result;
			}
			else {
				vector<Prototype> results(numThreads);
				int N = (int) kmers.size();

#pragma omp parallel for
				for ( int i = 0; i < numThreads; i++ ) {
					results[i].second = numeric_limits<double>::max();
				}

#pragma omp parallel for
				for ( int i = 0; i < N; i++ ) {
					int threadNum = omp_get_thread_num();
					auto & prototype = results[threadNum];

					Kmer & kmer = kmers[i];

					auto dist = DistanceTo(kmer);

					if ( dist < prototype.second ) {
						prototype.first = &kmer;
						prototype.second = dist;
					}
				}

				auto result = results[0];

				for ( int threadNum = 1; threadNum < numThreads; threadNum++ ) {
					auto & prototype = results[threadNum];

					if ( prototype.second < result.second ) {
						result = prototype;
					}
				}

				return result;
			}
		}

		/**
		**	Computes the information content of a cluster. This is necessarily
		**	going to be a bit shaky because some kmers belong to sequences that
		**	are members of multiple classes.
		**	I count these once for each distinct class.
		**	TODO: review literature for alternative measure of homogeneity.
		*/

		double Information(void) {
			double info = 0;

			if ( kmers.size() > 0 ) {
				int overallClassCount = FastaSequence::ClassNumberRegistry().size();
				int * freq = (int *) alloca(overallClassCount * sizeof(int));
				memset( freq, 0, overallClassCount * sizeof(int));
				int count = 0;

				for ( auto & kmer : kmers ) {
					for ( auto & classLabel : kmer.sequence->classNumbers ) {
						freq[classLabel] ++;
						count ++;
					}
				}

				for ( int i = 0; i < overallClassCount; i++ ) {
					if ( freq[i] > 0 ) {
						double p = (double) freq[i] / count;
						info += p * log(p) / log(2);
					}
				}
			}

			return -info;
		}

		/*
		**	Serializes the kmer strings to a stream. This does
		**	not preserve information about the sequences from which
		**	the kmers originated.
		*/

		void PrintKmers(FILE * f) {
			for ( int i = 0; i < kmers.size(); i++ ) {
				auto word = kmers[i].String();
				for ( int j = 0; j < kmers[i].length; j++ ) {
					fputc(word[j], f);
				}
				fputc('\n', f);
			}
		}
	};

	/**
	**  This is an equivalence relation. Complexity is linear in the size of the
	**  matrices. Matrices are considered equivalent if their dimensions are equal,
	**  and if corresponding elements compare equal.
	*/
	inline bool operator==(const KmerClusterAL & x, const KmerClusterAL & y) {
		return (x.kmers == y.kmers && x.pwm == y.pwm);
	}

	/**
	**  This is an equivalence relation. Complexity is linear in the size of the
	**  matrices. Matrices are considered equivalent if their dimensions are equal,
	**  and if corresponding elements compare equal.
	*/
	inline bool operator!=(const KmerClusterAL & x, const KmerClusterAL & y) {
		return x.kmers != y.kmers || x.pwm != y.pwm;
	}

	inline ostream & operator << (ostream & stream, const KmerClusterAL & cluster) {
		for ( int i = 0; i < cluster.kmers.size(); i++ ) {
			auto word = cluster.kmers[i].String();
			for ( int j = 0; j < cluster.kmers[i].length; j++ ) {
				stream << word[j];
			}
			stream << '\n';
		}

		return stream;
	}
}
