#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <cmath>
#include <functional>

#include "Alphabet.hpp"
#include "Array.hpp"
#include "EncodedKmer.hpp"
#include "Exception.hpp"
#include "Types.hpp"

using namespace std;
using namespace QutBio;

namespace QutBio {

	/// <summary> A call-back function used to process document fragments.
	/// </summary>
	/// <param name="fragmentIndex">
	///		The zero-origin index number of the current fragment within the seq.
	/// </param>
	/// <param name="fragmentCount">
	///		The number of fragments in the document.
	/// </param>
	/// <param name="subSequence">
	///		The text content of the current fragment.
	/// </param>

	using FragmentProcessor = function<void(
		size_t fragmentIndex,
		size_t fragmentCount,
		const char * subSequence,
		size_t len
		)>;

	/// <summary> A call-back function used to process subsequences.
	/// </summary>
	/// <param name="offset">
	///		The zero-origin position of the current fragment within the seq.
	/// </param>
	/// <param name="fragmentIndex">
	///		The zero origin index number of the subsequence as a fragment of 
	///		the containing sequence.
	/// </param>
	/// <param name="length">
	///		The length of the subsequence.
	/// </param>
	/// <param name="fragmentCount">
	///		The number of fragments in the document.
	/// </param>
	/// <param name="sequence">
	///		The sequence that contains the current fragment.
	/// </param>

	template <typename T>
	using SubsequenceProcessor = function<void(
		Array<T> & sequence,
		int offset,
		int length,
		int fragmentIndex,
		int fragmentCount
		)>;

	/// <summary> A set of functions used to facilitate document fragmentation.
	/// </summary>

	class Fragment {
	public:
		/// <summary>
		/// 
		/// </summary>
		/// <param name="seq">
		///		The sequence to be processed.
		/// </param>
		/// <param name="fragmentLength">
		///		The length of each fragment.
		/// </param>
		/// <param name="stepSize">
		///		The number of characters between fragment start points.
		/// </param>
		/// <param name="minLength">
		///		The minimum permitted fragment size.
		/// </param>
		/// <param name="process">
		///		A procedure to be carried out once for 
		/// </param>

		static void DissectString(
			const char * seq,
			size_t seqLen,
			uint fragmentLength,
			uint stepSize,
			uint minLength,
			FragmentProcessor process
			) {
			if ( fragmentLength < minLength ) {
				throw new Exception( "Argument Exception: fragmentLength", __FILE__, __LINE__ );
			}

			if ( stepSize <= 0 ) {
				throw new Exception( "Argument Exception: stepSize", __FILE__, __LINE__ );
			}

			if ( minLength <= 0 ) {
				throw new Exception( "Argument Exception: minLength", __FILE__, __LINE__ );
			}

			size_t fragmentCount = GetCount( seqLen, fragmentLength );

			for ( uint fragmentIndex = 0; fragmentIndex < fragmentCount; fragmentIndex++ ) {
				size_t start = fragmentIndex * stepSize;
				process( fragmentIndex, fragmentCount, seq + start, fragmentLength );
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="seq">
		///		The sequence to be processed.
		/// </param>
		/// <param name="fragmentLength">
		///		The length of each fragment.
		/// </param>
		/// <param name="stepSize">
		///		The number of characters between fragment start points.
		/// </param>
		/// <param name="minLength">
		///		The minimum permitted fragment size.
		/// </param>
		/// <param name="process">
		///		A procedure to be carried out once for 
		/// </param>
		/// <typeparam name="T">
		///		The element type of the sequence.
		/// </typeparam>

		template<typename T>
		static void DissectSequence(
			Array<T> & seq,
			int fragmentLength,
			int stepSize,
			int minLength,
			SubsequenceProcessor<T> process
			) {
			if ( fragmentLength < minLength ) {
				throw new Exception( "Argument Exception: fragmentLength", __FILE__, __LINE__ );
			}

			if ( stepSize <= 0 ) {
				throw new Exception( "Argument Exception: stepSize", __FILE__, __LINE__ );
			}

			if ( minLength <= 0 ) {
				throw new Exception( "Argument Exception: minLength", __FILE__, __LINE__ );
			}

			int fragmentCount = GetCount( seq.Length, fragmentLength, stepSize, minLength );

			for ( int fragmentIndex = 0; fragmentIndex < fragmentCount; fragmentIndex++ ) {
				int start = fragmentIndex * stepSize;
				process( seq, start, fragmentLength, fragmentIndex, fragmentLength );
			}
		}

		static size_t GetCount( size_t seqLength, size_t fragmentLength ) {
			uint fragmentCount = 0;

			if ( fragmentLength < seqLength ) {
				fragmentCount = (uint) ceil( (double) seqLength / fragmentLength );
			}
			else {
				fragmentCount = 1;
			}

			return fragmentCount;
		}

		static double GetRealStepSize(size_t seqLength, size_t fragLength, size_t fragCount) {
			return fragCount == 1 ? fragLength : (double) seqLength / fragCount;
		}

		static uint GetFragmentStart(uint idx, double stepSize, size_t kmerCount) {
			return min((uint) round( idx * stepSize), (uint) kmerCount );
		}

#ifdef WANT_ENCODED_KMER
		/// <summary> Partitions a sequence, and tiles the resulting subsequences into kmers.
		/// </summary>
		/// <param name="sequence"></param>
		/// <param name="fragLength"></param>
		/// <param name="fragInterval"></param>
		/// <param name="kmerLength"></param>
		/// <param name="charsPerWord">The number of characters packed into each word of the encoded kmer.</param>
		/// <param name="fragList">A preallocated array that will contain the fragments.</param>
		/// <param name="tileList">A preallocated two-dimensional array that will contain the collection of kmer tilings corresponding to the fragments.</param>
		/// <param name="alphabet">The alphabet used to encode kmers.</param>

		static void SplitAndTile(
			const char * sequence,
			size_t seqLen,
			Alphabet * alphabet,
			int fragLength,
			int fragInterval,
			int kmerLength,
			int charsPerWord,
			Array<const char *> & fragList,
			Array<Array<EncodedKmer>> & tileList

			) {

#pragma warning( disable:4100 )
			DissectString(sequence, seqLen, fragLength, fragInterval, kmerLength,
				[&fragList](int fragmentIndex, int fragmentCount, const char * subSequence, size_t len) {
				fragList[fragmentIndex] = subSequence;
			}
			);

			for ( uint i = 0; i < fragList.Length(); i++ ) {
				Array<EncodedKmer> & tiles = tileList[i];
				DissectString(fragList[i], fragLength, kmerLength, 1, 1,
					[&tiles, charsPerWord, alphabet](int fragmentIndex, int fragmentCount, const char * subSequence, size_t len) {
					alphabet->Encode(subSequence, len, charsPerWord, tiles[fragmentIndex]);
				}
				);
			}
		}

#endif // WANT_ENCODED_KMER

	};

}
