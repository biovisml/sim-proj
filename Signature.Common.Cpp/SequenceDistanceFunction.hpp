#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif


#include "Alphabet.hpp"
#include "SimilarityMatrix.hpp"
#include "FastaSequence.hpp"
#include "FreeList.hpp"
#include "Util.hpp"

#include <string>
#include <ctime>
#include <cfloat>
using namespace std;

#undef max
#undef min

namespace QutBio {

	class SequenceDistanceFunction {
	protected:
		SimilarityMatrix * matrix;
		uint kmerLength;

	public:

		SequenceDistanceFunction(
			SimilarityMatrix * matrix,
			uint kmerLength
			) :
			matrix(matrix),
			kmerLength(kmerLength) {}

		virtual ~SequenceDistanceFunction() {}

		virtual double ComputeDistance(
			FastaSequence * querySeq,
			FastaSequence * subjectSeq
			) = 0;
	};
}
