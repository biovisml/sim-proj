#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <string>
#include <ctime>
#include <cfloat>
#include <omp.h>
#include <numeric>
#include <unordered_set>

#include "Alphabet.hpp"
#include "Console.hpp"
#include "Delegates.hpp"
#include "FastaSequence.hpp"
#include "FragDistProcessor.hpp"
#include "FreeList.hpp"
#include "KmerCodebook.hpp"
#include "KmerSequenceRanker_Params.hpp"
#include "HausdorffCalculator.hpp"
#include "PointerList.hpp"
#include "BestHitDetector.hpp"
#include "Projector.hpp"
#include "ProjectorBitEmbedding.hpp"
#include "ProjectorSlice.hpp"
#include "Util.hpp"
#include "Ranking.hpp"
#include "kNearestNeighbours.hpp"

#define DO_OMP_TIMER 1
#include "OmpTimer.h"

#if defined(USE_AMP)
#include "ProjectorAMP.hpp"
#endif

#if defined(USE_BIG_CACHE)
#include "KmerDistanceCache.hpp"
#endif

using namespace std;

namespace QutBio {

	class KmerDistanceCalculator {
	protected:

		vector<Ranking> *rankings;
		int numThreads;
		function<void(vector<Ranking>& rankings)> queryComplete;
		function<void(void)> runComplete;

		Alphabet * alphabet;
		SimilarityMatrix * matrix;
		FragmentAggregationMode * kmerMode;
		FragmentAggregationMode * fragMode;

		// Values needed by the Hausdorff family of distance calculators.
		uint fragmentLength;
		uint interval;
		uint kmerLength;
		const vector<FastaSequence *> * queryDataset;
		const vector<FastaSequence *> * subjectDataset;
		Distance thresholdDistance = BAD_DIST;
		Distance defaultDistance = BAD_DIST;
		KmerCodebook * codebook = 0;
		bool pushKmerDistances;
		vector<FreeList<SequenceDistanceFunction>> distanceFunctions;
		size_t maxQueryKmerCount;
		size_t maxQueryFragCount;

		// TODO: Replace this with an arbitrary IKmerWordDistance function.
		KmerDistanceCache2 & cache;

		int skip;
		int maxRecords;

	public:
		KmerDistanceCalculator(
			SimilarityMatrix * matrix,
			FragmentAggregationMode * kmerMode,
			FragmentAggregationMode * fragMode,
			Alphabet * alphabet,
			uint fragLength,
			uint kmerLength,
			Distance thresholdDistance,
			Distance defaultDistance,
			bool pushKmerDistances,
			KmerDistanceCache2 & cache,
			int skip,
			int maxRecords
			) :
			cache(cache),
			matrix(matrix),
			alphabet(alphabet),
			fragmentLength(fragLength),
			interval(1),
			kmerLength(kmerLength),
			kmerMode(kmerMode),
			fragMode(fragMode),
			thresholdDistance(thresholdDistance),
			defaultDistance(defaultDistance),
			pushKmerDistances(pushKmerDistances),
			skip(skip),
			maxRecords(maxRecords) {

#pragma omp parallel
			numThreads = omp_get_num_threads();

			distanceFunctions.resize(numThreads);

			function<void(vector<Ranking>& rankings)> defaultQueryComplete = [&](vector<Ranking>& rankings) {};
			function<void(void)> defaultRunComplete = [&]() {};

			SetQueryComplete(defaultQueryComplete);
			SetRunComplete(defaultRunComplete);
		}

		virtual ~KmerDistanceCalculator() {}


		function<void(vector<Ranking>& rankings)> & QueryComplete(void) {
			return queryComplete;
		}

		void SetQueryComplete(function<void(vector<Ranking>& rankings)> & value) {
			queryComplete = value;
		}

		function<void(void)> RunComplete(void) {
			return runComplete;
		}

		void SetRunComplete(function<void(void)> value) {
			runComplete = value;
		}

		/// <summary>
		/// Called once before the start of all processing.
		/// </summary>

		virtual void setup(void) = 0;

		/// <summary>
		/// Called before each query is processed against the database.
		/// </summary>

		virtual void preProcess(FastaSequence * querySeq, int queryIdx) = 0;

		/// <summary>
		/// Called to process each (query seq, db seq) pair.
		/// </summary>

		virtual void process(FastaSequence * querySeq, size_t queryIdx, FastaSequence * subjectSeq, size_t subjectIdx, double distance) = 0;

		/// <summary>
		/// Called after all db sequences have been processed for given query sequence.
		/// </summary>

		virtual void postProcess(FastaSequence * querySeq, int queryIdx) = 0;

		/// <summary>
		/// Called once after all queries have been processed.
		/// </summary>

		virtual void cleanup(void) = 0;

		typedef KNearestNeighbours<Ranking> KnnRankings;

		/// Run the job.
		void RunJob(
			const vector<FastaSequence *> & query,
			const vector<FastaSequence *> & db
			) {

			queryDataset = &query;
			subjectDataset = &db;

			int maxQueryLength = numeric_limits<int>::min();

			for ( auto seq : query ) {
				int len = (int) seq->Sequence().size();

				if ( len > maxQueryLength ) maxQueryLength = len;
			}

			int maxSubjectLength = numeric_limits<int>::min();

			for ( auto seq : db ) {
				int len = (int) seq->Sequence().size();

				if ( len > maxSubjectLength ) maxSubjectLength = len;
			}

			maxQueryKmerCount = maxQueryLength - kmerLength + 1;

			maxQueryFragCount = (fragmentLength == 1 && skip > 1)
				? size_t(ceil((double) maxQueryKmerCount / skip))
				: Fragment::GetCount(maxQueryKmerCount, fragmentLength);

			const int maxSubjectKmerCount = maxSubjectLength - kmerLength + 1;

			setup();

			// Later we intend to sort the rankings, but sort is single-threaded.
			// So... accumulate rankings until we have enough to give a copy to each 
			// thread for sorting.
			vector<vector<Ranking>> pendingRankings(numThreads);
			int numPending = 0;

			KnnRankings knnAll(maxRecords);
			vector<KnnRankings> knnPerThread(numThreads);

			for ( int i = 0; i < numThreads; i++ ) {
				knnPerThread[i].setCapacity(maxRecords);
			}

			OMP_TIMER_DECLARE(overall);

			OMP_TIMER_START(overall);
			for ( size_t queryIdx = 0; queryIdx < query.size(); queryIdx++ ) {
				rankings = &pendingRankings[numPending++];

				FastaSequence * querySeq = query[queryIdx];
				preProcess(querySeq, (int) queryIdx);

				if ( codebook && !IS_BAD_DIST(thresholdDistance) ) {
					PopulateFragDistCache(querySeq, queryIdx, db);
				}
				else {
					DoPairwise(queryIdx, querySeq, db, maxQueryLength, maxSubjectLength, knnPerThread, knnAll);
				}

				postProcess(querySeq, (int) queryIdx);

				if ( numPending == numThreads ) {
					batchProcessRankings(pendingRankings, numPending);
					numPending = 0;
				}
			}

			if ( numPending > 0 ) {
				batchProcessRankings(pendingRankings, numPending);
			}
			OMP_TIMER_END(overall);

#if defined(DO_OMP_TIMER) && DO_OMP_TIMER
			cerr << "threads"
				<< "\t" << "Section 1"
				<< "\t" << "Section 2"
				<< "\t" << "Section 3"
				<< "\t" << "Section 4"
				<< "\t" << "Section 5"
				<< "\t" << "Section 6"
				<< "\t" << "Section bpr1"
				<< "\t" << "Section bpr2"
				<< "\t" << "Overall"
				<< endl;
			cerr << numThreads
				<< "\t" << OMP_TIMER(1)
				<< "\t" << OMP_TIMER(2)
				<< "\t" << OMP_TIMER(3)
				<< "\t" << OMP_TIMER(4)
				<< "\t" << OMP_TIMER(5)
				<< "\t" << OMP_TIMER(6)
				<< "\t" << OMP_TIMER(bpr1)
				<< "\t" << OMP_TIMER(bpr2)
				<< "\t" << OMP_TIMER(overall)
				<< endl;
#endif
			cleanup();
		}

		/**
		 **	Processes pending rankings collections a set of numThreads queries.
		 **	The sort is done in parallel, followed by a serial write operation.
		 **/

		void batchProcessRankings(
			vector<vector<Ranking>> &pendingRankings,
			int numberRemaining
			) {

			OMP_TIMER_START(bpr1);
#pragma omp parallel
			{
				int i = omp_get_thread_num();

				if ( i < numberRemaining ) {
					vector<Ranking> &rankings = pendingRankings[i];
					sort(rankings.begin(), rankings.end(), Ranking::AscendingDistance);
				}
			}
			OMP_TIMER_END(bpr1);

			OMP_TIMER_START(bpr2);
			{
				for ( int i = 0; i < numberRemaining; i++ ) {
					queryComplete(pendingRankings[i]);
				}
			}
			OMP_TIMER_END(bpr2);
		}

		const SimilarityMatrix & Matrix() {
			return *matrix;
		}

		typedef KmerCodebook::Cluster Cluster;

		OMP_TIMER_DECLARE(1);
		OMP_TIMER_DECLARE(2);
		OMP_TIMER_DECLARE(3);
		OMP_TIMER_DECLARE(4);
		OMP_TIMER_DECLARE(5);
		OMP_TIMER_DECLARE(6);
		OMP_TIMER_DECLARE(bpr1);
		OMP_TIMER_DECLARE(bpr2);

		vector<vector<uint8_t>> colMinima;
		vector<vector<uint8_t>> rowMinima;
		vector<FragDistProcessor_Frag<subvector<uint8_t>, uint8_t>> fragDistProcessor_frag;
		vector<FragDistProcessor_NoFrag<subvector<uint8_t>, uint8_t>> fragDistProcessor_noFrag;
		vector<omp_lock_t> dbLock;
		vector<bool> subjectSeen;
		vector<vector<size_t>> subjectOffset;
		vector<size_t> queryFragMapping;

		typedef pair<size_t, Cluster *> OffsetClusterMapping;

		// TODO: Replace this with a collection of priority queues
		vector<vector<OffsetClusterMapping>> relevantClustersPerThread;
		vector<OffsetClusterMapping *> relevantClusters;

		void PopulateFragDistCache(FastaSequence * querySeq, size_t queryIdx, const vector<FastaSequence *> & db) {
			static bool auxDataDone = false;
			bool isFirst = false;

			// ( cerr << "A" << endl ).flush();

			if ( !auxDataDone ) {
				auxDataDone = true;
				isFirst = true;
			}

			const size_t dbSize = db.size();
			const int T = numThreads;
			const int N = (int) codebook->kmerData.rows();

			// TODO: Convert Shift distance into a parameter or get rid of it.
			const int shift_distance = 1;

			const size_t maxPartSize = (dbSize + numThreads - 1) / numThreads;

			if ( isFirst ) {

				// Move this into an initialisation function elsewhere
				dbLock.resize(dbSize);
				fragDistProcessor_frag.resize(dbSize);
				colMinima.resize(numThreads);
				rowMinima.resize(numThreads);
				subjectOffset.resize(numThreads);
				vector<size_t> totLength(numThreads, 0);
				queryFragMapping.resize(maxQueryKmerCount);

#pragma omp parallel
				{
					int t = omp_get_thread_num();
					subjectOffset[t].resize(maxPartSize);

					for ( int n = 0; n < maxPartSize; n++ ) {
						size_t i = t * maxPartSize + n;

						if ( i >= dbSize ) break;

						FastaSequence * seq = db[i];
						const string & subjectStr = seq->Sequence();
						size_t subjectKmerCount = subjectStr.size() - kmerLength + 1;

						size_t subjectFragCount = Fragment::GetCount(subjectKmerCount, fragmentLength);

						subjectOffset[t][n] = totLength[t];
						totLength[t] += subjectFragCount;
					}

					colMinima[t].resize(totLength[t]);
					rowMinima[t].resize(maxPartSize * maxQueryFragCount);

					for ( int n = 0; n < maxPartSize; n++ ) {
						size_t i = t * maxPartSize + n;

						if ( i >= dbSize ) break;

						FastaSequence * seq = db[i];
						const string & subjectStr = seq->Sequence();
						size_t subjectKmerCount = subjectStr.size() - kmerLength + 1;
						size_t subjectFragCount = Fragment::GetCount(subjectKmerCount, fragmentLength);

						subvector<uint8_t> thisColMinima(&colMinima[t], subjectOffset[t][n], subjectFragCount);
						subvector<uint8_t> thisRowMinima(&rowMinima[t], n * maxQueryFragCount, maxQueryFragCount);

						FragDistProcessor_Frag<subvector<uint8_t>, uint8_t> & f(fragDistProcessor_frag[i]);
						f.rowMinima = thisRowMinima;
						f.colMinima = thisColMinima;
						f.queryKmerCount = maxQueryKmerCount;
						f.subjectKmerCount = subjectKmerCount;
						f.shift = shift_distance;

						f.queryFragCount = maxQueryFragCount;
						f.subjectFragCount = subjectFragCount;
						f.fragmentLength = fragmentLength;
						f.fragsPerTile = 1; // fragsPerTile;

						// TODO: Convert fragsPerTile to a parameter, or get rid of it.

						seq->position = i;

						omp_init_lock(&dbLock[i]);
					}
				}

				dbLock.resize(db.size());
				subjectSeen.resize(db.size());

				relevantClustersPerThread.resize(T);
				relevantClusters.reserve(N);
			}

			auto queryStr = querySeq->Sequence().c_str();
			size_t queryKmerCount = querySeq->Sequence().size() - kmerLength + 1;
			size_t queryFragCount = Fragment::GetCount(queryKmerCount, fragmentLength);

			for ( auto & relClust : relevantClustersPerThread ) {
				relClust.clear();
			}

			relevantClusters.clear();

#pragma omp parallel for
			for ( int i = 0; i < db.size(); i++ ) {
				fragDistProcessor_frag[i].queryFragCount = queryFragCount;
				fragDistProcessor_frag[i].queryKmerCount = queryKmerCount;
				subjectSeen[i] = false;
			}

			OMP_TIMER_START(1);
#pragma omp parallel
			{
				int t = omp_get_thread_num();
				int start = (N * t + T / 2) / T;
				int end = (N * (t + 1) + T / 2) / T;

				for ( int queryPos = 0; queryPos < (int) queryKmerCount; queryPos += skip ) {
					auto queryWord = querySeq->GetEncodedKmer(queryPos);

					for ( int codebookPos = start; codebookPos < end; codebookPos++ ) {
						EncodedKmer codeword = codebook->kmerData.row(codebookPos);

						if ( cache.GetDistance(queryWord, codeword, kmerLength) <= thresholdDistance ) {
							relevantClustersPerThread[t].emplace_back(queryPos, codebook->codebook[codebookPos]);
						}
					}
				}
			}
			OMP_TIMER_END(1);

			OMP_TIMER_START(2);
			for ( int t = 0; t < numThreads; t++ ) {
				for ( auto & hit : relevantClustersPerThread[t] ) {
					relevantClusters.push_back(&hit);
				}
			}
			OMP_TIMER_END(2);

			OMP_TIMER_START(3);
#pragma omp parallel for
			for ( int i = 0; i < relevantClusters.size(); i++ ) {
				int threadId = omp_get_thread_num();

				Cluster & cluster = *relevantClusters[i]->second;
				vector<Kmer> & items(cluster.items);

				size_t queryPos = relevantClusters[i]->first;
				auto queryWord = querySeq->GetEncodedKmer(queryPos);
				Distance distance = numeric_limits<Distance>::max();

				for ( size_t j = 0; j < cluster.Size(); j++ ) {
					Kmer & kmer = items[j];

					if ( !kmer.equalsPredecessor ) {
						// This test saves about 15% of total run time on SwissProt
						auto subjectWord = kmer.Word();
						distance = cache.GetDistance(queryWord, subjectWord, kmerLength);
					}

					if ( distance < defaultDistance ) {
						auto subjectSeq = kmer.sequence;
						auto subjectIdx = subjectSeq->position;
						FragDistProcessor_Frag<subvector<uint8_t>, uint8_t> & data = fragDistProcessor_frag[subjectIdx];

						omp_set_lock(&dbLock[subjectIdx]);

						if ( !subjectSeen[subjectIdx] ) {
							data.Reset(queryFragCount, defaultDistance);
							subjectSeen[subjectIdx] = true;
						}

						data.ProcessDistance(queryPos, items[j].kmerPosition, distance);

						omp_unset_lock(&dbLock[subjectIdx]);
					}
				}
			}
			OMP_TIMER_END(3);

			vector<size_t> relevantSubjectIndices;

			OMP_TIMER_START(4);
			for ( size_t i = 0; i < db.size(); i++ ) {
				if ( subjectSeen[i] ) relevantSubjectIndices.push_back(i);
			}
			OMP_TIMER_END(4);

			int64_t relevantSubjectCount = relevantSubjectIndices.size();
			vector<vector<Ranking>> rankingsPerThread(numThreads);

			OMP_TIMER_START(5);
#pragma omp parallel for
			for ( int64_t n = 0; n < relevantSubjectCount; n++ ) {
				int t = omp_get_thread_num();
				size_t i = relevantSubjectIndices[n];
				auto subjectSeq = db[i];

				FragDistProcessor_Frag<subvector<uint8_t>, uint8_t> & data = fragDistProcessor_frag[i];

				// TODO: Move this into the FragDistProcessor, where I'm sure there is enough information to handle it.
				double distance = FragHausdorffAverageAverage_FragDistCache(
					data.rowMinima,
					data.colMinima,
					queryFragCount,
					queryKmerCount,
					data.subjectFragCount,
					data.subjectKmerCount,
					fragmentLength,
					shift_distance
					);

				rankingsPerThread[t].emplace_back(querySeq, subjectSeq, distance, 0, false);
			}
			OMP_TIMER_END(5);

			OMP_TIMER_START(6);
			for ( int t = 0; t < numThreads; t++ ) {
				rankings->insert(rankings->end(), rankingsPerThread[t].begin(), rankingsPerThread[t].end());
			}
			OMP_TIMER_END(6);
		}

		template<typename vectorType>
		double FragHausdorffAverageAverage_FragDistCache(
			vectorType & rowMinima,
			vectorType & colMinima,
			size_t queryFragCount,
			size_t queryKmerCount,
			size_t subjectFragCount,
			size_t subjectKmerCount,
			int fragmentLength,
			int shift
			) {
			int totXY = 0;
			int totYX = 0;

			int queryQuotient = (int) queryKmerCount / fragmentLength;
			int queryRemainder = (int) queryKmerCount % fragmentLength;
			int queryWeight = queryQuotient * fragmentLength + queryRemainder;

			int subjectQuotient = (int) subjectKmerCount / fragmentLength;
			int subjectRemainder = (int) subjectKmerCount % fragmentLength;
			int subjectWeight = subjectQuotient * fragmentLength + subjectRemainder;

			// Compute average one-way distance from query to subject, weighting 
			//	each fragment by the number of kmers contained therein.
			for ( int i = 0; i < queryQuotient; i++ ) {
				totXY += rowMinima[i] << shift;
			}

			totXY *= fragmentLength;

			if ( queryRemainder ) totXY += rowMinima[queryQuotient] * queryRemainder;

			// Compute average one-way distance from subject to query, weighting 
			//	each fragment by the number of kmers contained therein.
			for ( int j = 0; j < subjectQuotient; j++ ) {
				totYX += colMinima[j] << shift;
			}

			totYX *= fragmentLength;

			if ( subjectRemainder ) totYX += colMinima[subjectQuotient] * subjectRemainder;

			double avgXY = (double) totXY / queryWeight;
			double avgYX = (double) totYX / subjectWeight;

			double result = (avgXY + avgYX) / 2;
			return result;
		}

		void DoPairwise(
			size_t queryIdx,
			FastaSequence *querySeq,
			const vector<FastaSequence *> & db,
			size_t maxQueryLength,
			size_t maxSubjectLength,
			vector<KnnRankings> &knnPerThread,
			KnnRankings &knnAll
			) {
			for ( KnnRankings & knn : knnPerThread ) {
				knn.clear();
			}

			for ( int subjectIdx = 0; subjectIdx < db.size(); subjectIdx++ ) {
				int thread = omp_get_thread_num();
				KnnRankings & knn = knnPerThread[thread];
				FreeList<SequenceDistanceFunction> & sequenceDistances = distanceFunctions[thread];
				FastaSequence * subjectSeq = db[subjectIdx];
				DoPairwise(queryIdx, querySeq, maxQueryLength, subjectIdx, subjectSeq, maxSubjectLength, knn, sequenceDistances);
			}

			// Now get the maxRecords nearest overall.
			{
				knnAll.clear();

				for ( KnnRankings & knn : knnPerThread ) {
					for ( Ranking & r : knn ) {
						knnAll.push(r);
					}
				}
			}

			// Copy maxRecords nearest to the pending rankings vector.
			{
				rankings->clear();
				rankings->insert(rankings->end(), knnAll.begin(), knnAll.end());
			}
		}

		void DoPairwise(
			size_t queryIdx,
			FastaSequence * querySeq,
			size_t maxQueryLength,
			size_t subjectIdx,
			FastaSequence * subjectSeq,
			size_t maxSubjectLength,
			KNearestNeighbours<Ranking> & knn,
			FreeList<SequenceDistanceFunction> & distanceFunctions
			) {
			function< SequenceDistanceFunction * (void) > factory;

			if ( kmerMode == FragmentAggregationMode::Projector() ) {
				factory = [=]() {
					return (SequenceDistanceFunction *) new Projector(matrix, kmerLength, fragMode);
				};
			}
			else if ( kmerMode == FragmentAggregationMode::ProjectorBitEmbedding() ) {
				factory = [=]() {
					return (SequenceDistanceFunction *) new ProjectorBitEmbedding(matrix, kmerLength, fragMode);
				};
			}
			else if ( kmerMode == FragmentAggregationMode::ProjectorSlice() ) {
				factory = [=]() {
					return (SequenceDistanceFunction *) new ProjectorSlice(matrix, kmerLength, fragMode, interval);
				};
			}
			else if ( kmerMode == FragmentAggregationMode::BestOfBest() && fragMode == FragmentAggregationMode::BestOfBest() ) {
				factory = [=]() {
					return (SequenceDistanceFunction *) new BestHitDetector(matrix, kmerLength);
				};
			}
			else {
				factory = [=]() {
					auto calc = new HausdorffCalculator(
						matrix, kmerLength, kmerMode, fragMode, alphabet, fragmentLength, maxQueryLength, maxSubjectLength, pushKmerDistances
						);

					if ( !IS_BAD_DIST(thresholdDistance) ) {
						calc->SetThreshold(thresholdDistance, defaultDistance);
					}

					return (SequenceDistanceFunction *) calc;
				};
			}

			auto calculator = distanceFunctions.Allocate(factory);

			double distance = calculator->ComputeDistance(querySeq, subjectSeq);

			Ranking r(querySeq, subjectSeq, distance, 0, false);
			knn.push(r);

			distanceFunctions.Free(calculator);
		}
	};
}
