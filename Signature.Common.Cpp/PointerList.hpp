#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <vector>
#include <iterator>

#include "Assert.hpp"
#include "Delegates.hpp"

using namespace std;
using namespace QutBio;

namespace QutBio {
	/**
	<summary>
		A lookup table mapping some key of type K to _pointer_to_ values of type V, which owns the stored pointers.
		The values are dynamically allocated when a value is added by a factory function which is responsible for
		correct initialisation.
		When the collection is deleted the pointers are destroyed.
	<summary>
		*/

	template<typename V>
	class PointerList {
	private:
		vector<V *> vec;
	public:
		typedef V *Pointer;

		PointerList() {}

		~PointerList() {
		//#pragma omp parallel for
		//	for ( int i = 0; i < vec.size(); i++ ) {
		//		delete vec[i];
		//	}
		}

		PointerList( const PointerList & other ) = delete;

		PointerList & operator = ( const PointerList & other ) = delete;

		const vector<V *> & Items(void) { return vec; }

		/**
			Uses the supplied factory function to create a pointer to new dynamically
			allocated object. The pointer is appended to the current list. Later, the 
			pointer will be disposed via the delete operator, so
			the dynamic allocation needs to be essentially a single call to new,
			followed by initialisation.

			DO NOT USE ADD to append shallow copies of objects, or youi will get a
			"dual call to free" event!.
		 */
		void Add( Func<V *> factory ) {
			V * value = factory();
			vec.push_back( value );
		}

		size_t Length() {
			return vec.size();
		}

		V * operator[] ( size_t index ) {
			return vec[ index ];
		}

		void ForEach( Action1<V *> action ) {
			for ( auto v : vec ) {
				action( v );
			}
		}

		typename vector<Pointer>::iterator begin() {
			return vec.begin();
		}

		typename vector<Pointer>::iterator end() {
			return vec.end();
		}
	};
}
