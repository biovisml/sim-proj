#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

namespace QutBio {
    class Distribution {
    public:
        virtual ~Distribution() {}

        virtual double Cdf(double t) = 0;
        virtual double Pdf(double t) = 0;
        virtual double InverseCdf(double t) = 0;
        virtual double Mean( void ) = 0;
        virtual double StdDev( void ) = 0;
    };
}
