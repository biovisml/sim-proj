#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <algorithm>
#include <ios>
#include <string>

#include "IArrayParser.hpp"

using namespace std;

namespace QutBio {

	/// <summary> A ranked document result.
	/// </summary>

	struct Ranking : public IArrayParser {
		/// <summary> The document ID of the ranked document. 
		/// </summary>

	public:
		FastaSequence *query;
		FastaSequence *subject;
		double distance;
		size_t rank;
		bool isRelevant;

	public:
		Ranking(
			FastaSequence *query,
			FastaSequence *subject,
			double distance,
			size_t rank,
			bool isRelevant
			) :
			query(query),
			subject(subject),
			distance(distance),
			rank(rank),
			isRelevant(isRelevant)

		{}

		// For serialization only.
		Ranking() {}

		/// <summary> Orders two ranking objects in ascending order of distance.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>

		// typedef Ranking * pRanking;

		static bool AscendingDistance(const Ranking & x, const Ranking & y) {
			return x.distance < y.distance;
		}

		friend bool operator< (const Ranking & x, const Ranking & y) {
			return x.distance < y.distance;
		}

		string ToString() {
			ostringstream writer;
			CsvWriter csv(writer);
			vector<string> fields;
			ToStringArray(fields);
			csv.WriteRecord(fields);
			return writer.str();
		}

		/// <summary> Returns an array of string which contains the fields, in order. 
		/// <para>Any missing values are represented by a single '?' character.</para>
		/// </summary>
		/// <returns></returns>

		void ToStringArray(vector<string> & record) {
			ostringstream distStr;
			distStr << distance;

			ostringstream rankStr;
			rankStr << rank;

			record.clear();
			record.push_back(query ? query->Id() : "?");
			record.push_back(subject->Id());
			record.push_back(distStr.str());
			record.push_back(rankStr.str());
			record.push_back(isRelevant ? "true" : "false");
		}

		/**
		 ** Serialise a ranking object to text format suitable for
		 **	trec_eval. Distance is negated to give a "similarity".
		 */

		friend ostream & operator<<(ostream & out, const Ranking & ranking) {
			out << (ranking.query ? ranking.query->Id() : "?") 
				<< " 0 "
				<< ranking.subject->Id() 
				<< " 0 "
				<< (-ranking.distance) 
				<< " ignored";

			return out;
		}

		/// <summary>
		/// Returns a new Ranking record extracted from an array of strings which represent
		/// the fields of a Ranking record.
		/// </summary>
		/// <param name="record"></param>
		/// <returns></returns>

		void Parse(vector<string> & record) {
			throw NotImplementedException(FileAndLine);
		}

		static int CompareByDistance(const void * r1, const void * r2) {
			Ranking **x = (Ranking **) r1;
			Ranking **y = (Ranking **) r2;
			double xd = (**x).distance;
			double yd = (**y).distance;

			return xd < yd ? -1 : xd > yd ? +1 : 0;
		}

	};
}
