#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <string>
#include <ctime>
#include <cfloat>
#include <limits>

#include "Alphabet.hpp"
#include "Console.hpp"
#include "Delegates.hpp"
#include "EncodedKmer.hpp"
#include "FastaSequence.hpp"
#include "Fragment.hpp"
#include "KmerSequenceRanker_Params.hpp"
#include "PointerList.hpp"
#include "Ranking.hpp"
#include "AveragePrecision.hpp"
#include "Util.hpp"

#include "KmerDistanceCalculator.hpp"

using namespace std;

namespace QutBio {

	class KmerSequenceRanker : public KmerDistanceCalculator {

	public:
		KmerSequenceRanker(
			/**/	SimilarityMatrix * matrix,
			/**/	FragmentAggregationMode * kmerMode,
			/**/	FragmentAggregationMode * fragMode,
			/**/	Alphabet * alphabet,
			/**/	uint fragLength,
			/**/	uint kmerLength,
			/**/	Distance thresholdDistance,
			/**/	Distance defaultDistance,
			/**/	bool pushKmerDistances,
			/**/	KmerDistanceCache2 & cachedCalculator,
			/**/	int skip,
			/**/	int maxRecords
			) :
			KmerDistanceCalculator(
			/**/	matrix,
			/**/	kmerMode,
			/**/	fragMode,
			/**/	alphabet,
			/**/	fragLength,
			/**/	kmerLength,
			/**/	thresholdDistance,
			/**/	defaultDistance,
			/**/	pushKmerDistances,
			/**/	cachedCalculator,
			/**/	skip,
			/**/	maxRecords
			) {
		}

		void SetThreshold( Distance thresholdDistance, Distance defaultDistance ) {
			assert_false( IS_BAD_DIST( thresholdDistance ) );
			assert_false( IS_BAD_DIST( defaultDistance ) );
			assert_true( thresholdDistance <= defaultDistance );

			this->thresholdDistance = thresholdDistance;
			this->defaultDistance = defaultDistance;
		}

		void SetCodebook( KmerCodebook * codebook ) {
			this->codebook = codebook;
		}

		double ThresholdDistance() { return thresholdDistance; }

		double DefaultDistance() { return defaultDistance; }

		virtual ~KmerSequenceRanker() {
		}

		void setup( void ) {}

		void preProcess( FastaSequence * querySeq, int queryIdx ) {
			rankings->clear();
		}

		void process( FastaSequence * querySeq, size_t queryIdx, FastaSequence * subjectSeq, size_t subjectIdx, double distance ) {
			rankings->emplace_back(
				querySeq,
				subjectSeq,
				distance,
				0, // rank
				false // isRelevant
			);
		}


		void postProcess( FastaSequence * querySeq, int queryIdx ) {
		}

		void cleanup( void ) {
			runComplete();
		}


	};
}
