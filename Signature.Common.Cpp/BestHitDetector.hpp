#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif


#include "Alphabet.hpp"
#include "SimilarityMatrix.hpp"
#include "FastaSequence.hpp"
#include "FreeList.hpp"
#include "Util.hpp"
#include "SequenceDistanceFunction.hpp"

#include <string>
#include <ctime>
#include <cfloat>
using namespace std;

namespace QutBio {

	class BestHitDetector: SequenceDistanceFunction {
	protected:
		/// <summary>
		///		Row and column minima
		///	</summary>
		Distance bestDistance;

		// Query sequence data.
		FastaSequence * querySeq;
		const char * queryChars;
		size_t queryIdx;
		size_t queryKmerCount;

		// Subject (DB) sequence data.
		FastaSequence * subjectSeq;
		const char * subjectChars;
		size_t subjectIdx;
		size_t subjectKmerCount;

	public:
		BestHitDetector(
			SimilarityMatrix * matrix,
			uint kmerLength
			) : SequenceDistanceFunction( matrix, kmerLength ) {}

		virtual ~BestHitDetector() {}

		double ComputeDistance(
			FastaSequence * querySeq,
			FastaSequence * subjectSeq
			) {
			queryChars = querySeq->Sequence().c_str();
			queryKmerCount = querySeq->Sequence().size() - kmerLength + 1;

			subjectChars = subjectSeq->Sequence().c_str();
			subjectKmerCount = subjectSeq->Sequence().size() - kmerLength + 1;

			UpdateKmerDistCache();

			return bestDistance;
		}

	private:

		/// <summary>
		///	Given strings s and t, having length m and n respectively, and a similarity matrix 
		///	reformatted as a 128 by 128 array of integers, populates a vector contain a kmer mutual
		///	distance table.
		/// </summary>
		///	<param name="queryChars"></param>

		void UpdateKmerDistCache() {
			const SimilarityMatrix * matrix = this->matrix;
			const Distance max = matrix->MaxValue();
			const size_t m = queryKmerCount;
			const size_t n = subjectKmerCount;

			bestDistance = numeric_limits<Distance>::max();

			for ( size_t r = 0; r < m; r++ ) {
				const int c_upper = r == 0 ? (int) n : 1;

				//	Do the top-right part of the rectangle
				for ( int c = 0; c < c_upper; c++ ) {
					Distance buffer[1000];
					const char * a = subjectChars + c;
					const char * b = queryChars + r;
					Distance distance = 0;

					size_t diagLength = m - r;

					if ( n - c < diagLength ) {
						diagLength = n - c;
					}

					// Prime the circular buffer with the first kmer in the query
					for ( size_t t = 0; t < kmerLength; t++, a++, b++ ) {
						Distance currentTerm = max - matrix->dict[*a][*b];
						distance += currentTerm;
						buffer[t] = currentTerm;
					}

					if ( distance < bestDistance ) bestDistance = distance;

					for ( size_t offset = 1, buffptr = 0;
						offset < diagLength;
						a++, b++, offset++, buffptr++
						) {
						if ( buffptr >= kmerLength ) {
							buffptr = 0;
						}

						distance -= buffer[buffptr];
						Distance currentTerm = max - matrix->dict[*a][*b];
						buffer[buffptr] = currentTerm;
						distance += currentTerm;

						if ( distance < bestDistance ) bestDistance = distance;
					}
				}
			}
		}

	};
}
