#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <fstream>
#include <sstream>
#include <cstring>
#include <string>
#include <cstdint>
#include <functional>

#include "Types.hpp"
#include "Exception.hpp"

using namespace std;

namespace QutBio {

	template <typename T>
	T min(T x, T y) { return x < y ? x : y; }

	template <typename T>
	T max(T x, T y) { return x > y ? x : y; }

	template<typename T>
	static T sum(typename vector<T>::iterator begin, typename vector<T>::iterator end, T initial) {
		T total = initial;

		for ( auto iter = begin; iter != end; iter++ ) {
			total += *iter;
		}

		return total;
	}

	class Int {
	public:
		static int Parse(const string & s) {
			istringstream str(s);
			int result;
			str >> result;
			return result;
		}

		static string ToString(int value) {
			ostringstream str;
			str << value;
			return str.str();
		}

		static string Join(const vector<int> & x, string & delimiter) {
			ostringstream s;

			if ( x.size() > 0 ) {
				s << x[0];

				for ( auto current = x.begin() + 1; current != x.end(); current++ ) {
					s << delimiter << *current;
				}
			}

			return s.str();
		}
	};

	class Uint {
	public:
		static uint Parse(const string & s) {
			istringstream str(s);
			uint result;
			str >> result;
			return result;
		}

		static string ToString(uint value) {
			ostringstream str;
			str << value;
			return str.str();
		}

		static string Join(const vector<uint> & x, string & delimiter) {
			ostringstream s;

			if ( x.size() > 0 ) {
				s << x[0];

				for ( auto current = x.begin() + 1; current != x.end(); current++ ) {
					s << delimiter << *current;
				}
			}

			return s.str();
		}
	};

	class Uint64 {
	public:
		static uint64_t Parse(const string & s) {
			istringstream str(s);
			uint64_t result;
			str >> result;
			return result;
		}

		static string ToString(uint64_t value) {
			ostringstream str;
			str << value;
			return str.str();
		}

		static string Join(const vector<uint64_t> & x, string & delimiter) {
			ostringstream s;

			if ( x.size() > 0 ) {
				s << x[0];

				for ( auto current = x.begin() + 1; current != x.end(); current++ ) {
					s << delimiter << *current;
				}
			}

			return s.str();
		}
	};

	template <typename T>
	class Convert {
	public:
		static T Parse(const string & s) {
			istringstream str(s);
			T result;
			str >> result;
			return result;
		}

		static string ToString(T value) {
			ostringstream str;
			str << value;
			return str.str();
		}

		static string Join(const vector<T> & x, string & delimiter) {
			ostringstream s;

			if ( x.size() > 0 ) {
				s << x[0];

				for ( auto current = x.begin() + 1; current != x.end(); current++ ) {
					s << delimiter << *current;
				}
			}

			return s.str();
		}
	};

	class Ulong {
	public:
		static ulong Parse(const string & s) {
			istringstream str(s);
			uint result;
			str >> result;
			return result;
		}

		static string ToString(ulong value) {
			ostringstream str;
			str << value;
			return str.str();
		}

		static string Join(const vector<ulong> & x, string & delimiter) {
			ostringstream s;

			if ( x.size() > 0 ) {
				s << x[0];

				for ( auto current = x.begin() + 1; current != x.end(); current++ ) {
					s << delimiter << *current;
				}
			}

			return s.str();
		}
	};

	class Double {
		/**
		*	<summary>
		*	Scans a floating point value from a supplied string.
		*	</summary>
		*	<param name="s">The string whichis isupposed to contain text
		*	representation of a floating point value.</param>
		*	<returns>
		*	</returns>
		*/
	public:
		static double Parse(const string & s) {
			istringstream str(s);
			double result;
			str >> result;
			return result;
		}

		static string ToString(double value) {
			ostringstream str;
			str << value;
			return str.str();
		}

		static string Join(const vector<double> & x, string & delimiter) {
			ostringstream s;

			if ( x.size() > 0 ) {
				s << x[0];

				for ( auto current = x.begin() + 1; current != x.end(); current++ ) {
					s << delimiter << *current;
				}
			}

			return s.str();
		}
	};

	class Bool {
	public:
		static bool Parse(const string & s) {
			const char
				* t = "true",
				*cs = s.c_str();

			for ( ; *cs && *t; cs++, t++ ) {
				if ( tolower(*cs) != *t ) return false;
			}

			return *cs == 0 && *t == 0;
		}

		static string ToString(double value) {
			ostringstream str;
			str << (value ? "true" : "false");
			return str.str();
		}

		static string Join(const vector<bool> & x, string & delimiter) {
			ostringstream s;

			if ( x.size() > 0 ) {
				s << x[0];

				for ( auto current = x.begin() + 1; current != x.end(); current++ ) {
					s << delimiter << (*current ? 1 : 0);
				}
			}

			return s.str();
		}
	};

	class File {
	public:
		static bool Exists(const string & fileName) {
			FILE * f = fopen(fileName.c_str(), "r");
			if ( f ) {
				fclose(f);
				return true;
			}
			return false;
		}

		static void ReadStrings( istream & str, function<void(const string & s)> action ) {
			string s;
			while ( std::getline(str, s) ) {
				action(s);
			}
		}

		static void ReadStrings( const string & fileName, function<void(const string & s)> action ) {
			ifstream str( fileName );
			ReadStrings( str, action );
		}
	};
}
