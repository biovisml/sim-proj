#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <stdint.h>
#include "Array.hpp"

namespace QutBio {
	typedef uint16_t KmerWord;
	typedef KmerWord * EncodedKmer;
}
