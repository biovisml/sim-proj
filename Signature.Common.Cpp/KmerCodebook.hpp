#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <map>
#include <set>
#include <vector>
#include <iostream>
#include <functional>
#include <numeric>
#include <math.h>

#include "FastaSequence.hpp"
#include "Kmer.hpp"
#include "SimilarityMatrix.hpp"
#include "Selector.hpp"
#include "KmerDistanceCache.hpp"

using namespace std;

namespace QutBio {

	class KmerCodebook {
	public:
		enum InitMode {
			Uniform,
			LinearPerCluster,
			LogPerCluster
		};

		class Cluster : public Kmer {
		public:
			vector<Kmer> items;
			size_t expectedSize;
			size_t index;

			Cluster(FastaSequence * seq, size_t position, size_t kmerLength, size_t expectedSize) :
				Kmer(seq, position, kmerLength), expectedSize(expectedSize) {}

			virtual ~Cluster() {}

			FlatMatrix<KmerWord> kmerData;

			void CopyKmerData(int wordsPerKmer) {
				auto N = items.size();
				kmerData.resize(N, wordsPerKmer);

				for ( size_t i = 0; i < N; i++ ) {
					memcpy(kmerData.row(i), items[i].Word(), wordsPerKmer * sizeof(KmerWord));
				}
			}

			friend ostream & operator<<(ostream & str, const Cluster & cluster) {
				str << "Cluster" << "," << cluster.items.size() << "," << (Kmer) cluster << endl;

				for ( uint i = 0; i < cluster.items.size(); i++ ) {
					str << (cluster.items[i]) << endl;
				}

				return str;
			}

			void Add(FastaSequence *seq, size_t pos, size_t kmerLength) {
				items.emplace_back(Kmer(seq, pos, kmerLength));
			}

			size_t Size() {
				return items.size();
			}

			vector<Kmer>::iterator begin() {
				return items.begin();
			}

			vector<Kmer>::iterator  end() {
				return items.end();
			}
		};

	public:
		size_t codebook_size;
		vector<Cluster *> codebook;
		KmerDistanceCache2 &cache;
		uint kmerLength;
		PointerList<FastaSequence> & db;
		bool splitLargeClusters;
		FastaSequence::Index idx;

	public:
		FlatMatrix<KmerWord> kmerData;

		KmerCodebook(
			//--------------------------------------	
			KmerDistanceCache2 &cache,
			uint kmerLength,
			size_t codebook_size,
			PointerList<FastaSequence> & db,
			UniformRealRandom & rand,
			bool splitLargeClusters = false,
			InitMode initMode = InitMode::Uniform
			//--------------------------------------
			) :
			cache(cache),
			kmerLength(kmerLength),
			codebook_size(codebook_size),
			db(db),
			splitLargeClusters(splitLargeClusters),
			idx(db.Items())
			//-------------------------------------
		{
			if ( initMode == InitMode::Uniform ) {
				SelectPrototypesUniform(rand);
			}
			else if ( initMode == InitMode::LinearPerCluster ) {
				SelectPrototypesLinear(rand);
			}
			else {
				SelectPrototypesLog(rand);
			}

			{ fprintf(stderr, "%s(%d): codebook.size() = %zu\n", FileAndLine, codebook.size()); fflush(stderr); }

			int wordsPerKmer = (int) Alphabet::WordsPerKmer(kmerLength, cache.CharsPerWord());
			CopyKmerDataNonRecursive(wordsPerKmer);
			{ fprintf(stderr, "%s(%d): codebook.size() = %zu\n", FileAndLine, codebook.size()); fflush(stderr); }

			AddWordsToClusters2();
			{ fprintf(stderr, "%s(%d): codebook.size() = %zu\n", FileAndLine, codebook.size()); fflush(stderr); }
			IdentifySharedKmers();
			{ fprintf(stderr, "%s(%d): codebook.size() = %zu\n", FileAndLine, codebook.size()); fflush(stderr); }
		}

		void SelectPrototypesUniform(
			//------------------------------------
			UniformRealRandom & rand
			//------------------------------------
			) {
			size_t kmerCount = FastaSequence::GetTotalKmerCount(db, kmerLength);

			Selector s(rand, (uint) codebook_size, (uint) kmerCount);

			fprintf(stderr, "codebook_size = %zu\n", codebook_size);
			fprintf(stderr, "kmerCount = %zu\n", kmerCount);
			fflush(stderr);

			size_t total = 0;

			for ( auto seq : db ) {
				size_t n = seq->Sequence().size() - kmerLength + 1;
				total += n;

				for ( size_t i = 0; i < n; i++ ) {
					if ( s.SelectThis() ) {
						Cluster * newCluster = new Cluster(seq, i, kmerLength, 0);
						newCluster->index = codebook.size();
						codebook.push_back(newCluster);
					}
				}
			}

			if ( total != kmerCount ) {
				fprintf(stderr, "total = %zu\n", total);
				fprintf(stderr, "This does not match the required number. Exiting now.\n");
				exit(1);
			}

			if ( codebook.size() != codebook_size ) {
				fprintf(stderr, "codebook.size() = %zu\n", codebook.size());
				fprintf(stderr, "This does not match the required number. Exiting now.\n");
				exit(1);
			}
		}

		void SelectPrototypesLinear(
			//------------------------------------
			UniformRealRandom & rand
			//------------------------------------
			) {
			auto&& classRegistry = FastaSequence::ClassNumberRegistry();
			vector<vector<Kmer>> kmersPerClass(classRegistry.size());

			for ( auto seq : db.Items() ) {
				for ( auto family : seq->classNumbers ) {
					seq->SelectKmers(kmerLength, [&kmersPerClass, family](FastaSequence * s, size_t pos, size_t len) {
						kmersPerClass[family].emplace_back(s, pos, len);
					});
				}
			}

			Kmer zero = Kmer::Zero(kmerLength);

			size_t distinctKmerCount = 0;

			const size_t C = kmersPerClass.size();

			for ( size_t c = 0; c < C; c++ ) {
				// akif == all kmers in family
				vector<Kmer> & akif(kmersPerClass[c]);

				if ( akif.size() == 0 ) continue;

				std::sort(akif.begin(), akif.end());

				Kmer * previous = &zero;
				size_t outIdx = 0;
				const size_t M = akif.size();

				for ( size_t m = 0; m < M; m++ ) {
					Kmer * kmer = &akif[m];

					if ( !(*kmer == *previous) ) {
						akif[outIdx++] = *kmer;
						previous = kmer;
					}
				}

				akif.resize(outIdx);
				distinctKmerCount += outIdx;
			}

			if ( 1 ) {
				for ( size_t c = 0; c < C; c++ ) {
					vector<Kmer> & akif = kmersPerClass[c];
					const size_t M = akif.size();

					for ( size_t m = 1; m < M; m++ ) {
						Assert::IsTrue(akif[m - 1] < akif[m], FileAndLine);
					}
				}
			}

			for ( auto&& akif : kmersPerClass ) {
				size_t classCount = akif.size();
				size_t classWanted = (classCount * codebook_size + distinctKmerCount - 1) / distinctKmerCount;

				if ( classWanted > classCount ) classWanted = classCount;

				Selector s(rand, classWanted, classCount);

				for ( size_t i = 0; i < classCount; i++ ) {
					if ( s.SelectThis() ) {
						Kmer & k = akif[i];
						Cluster * newCluster = new Cluster(k.sequence, k.kmerPosition, kmerLength, 0);
						newCluster->index = codebook.size();
						codebook.push_back(newCluster);
					}
				}
			}

			codebook_size = codebook.size();
		}

		void SelectPrototypesLog(
			//------------------------------------
			UniformRealRandom & rand
			//------------------------------------
			) {
			auto&& classRegistry = FastaSequence::ClassNumberRegistry();
			vector<vector<Kmer>> kmersPerClass(classRegistry.size());

			for ( auto seq : db.Items() ) {
				for ( auto family : seq->classNumbers ) {
					seq->SelectKmers(kmerLength, [&kmersPerClass, family](FastaSequence * s, size_t pos, size_t len) {
						kmersPerClass[family].emplace_back(s, pos, len);
					});
				}
			}

			Kmer zero = Kmer::Zero(kmerLength);

			size_t distinctKmerCount = 0;

			const size_t C = kmersPerClass.size();

			for ( size_t c = 0; c < C; c++ ) {
				// akif == all kmers in family
				vector<Kmer> & akif(kmersPerClass[c]);

				if ( akif.size() == 0 ) continue;

				std::sort(akif.begin(), akif.end());

				Kmer * previous = &zero;
				size_t outIdx = 0;
				const size_t M = akif.size();

				for ( size_t m = 0; m < M; m++ ) {
					Kmer * kmer = &akif[m];

					if ( !(*kmer == *previous) ) {
						akif[outIdx++] = *kmer;
						previous = kmer;
					}
				}

				akif.resize(outIdx);
				distinctKmerCount += outIdx;
			}

			if ( 1 ) {
				for ( size_t c = 0; c < C; c++ ) {
					vector<Kmer> & akif = kmersPerClass[c];
					const size_t M = akif.size();

					for ( size_t m = 1; m < M; m++ ) {
						Assert::IsTrue(akif[m - 1] < akif[m], FileAndLine);
					}
				}
			}

			for ( auto&& kmersInFamily : kmersPerClass ) {
				size_t classCount = kmersInFamily.size();
				size_t classWanted = (size_t) ceil(log(classCount) * codebook_size / log(distinctKmerCount));

				if ( classWanted > classCount ) classWanted = classCount;

				Selector s(rand, classWanted, classCount);

				for ( size_t i = 0; i < classCount; i++ ) {
					if ( s.SelectThis() ) {
						Kmer & k = kmersInFamily[i];
						Cluster * newCluster = new Cluster(k.sequence, k.kmerPosition, kmerLength, 0);
						newCluster->index = codebook.size();
						codebook.push_back(newCluster);
					}
				}
			}

			codebook_size = codebook.size();
		}

		KmerCodebook(
			//---------------------------------------
			KmerDistanceCache2 &cache,
			uint kmerLength,
			PointerList<FastaSequence> &db,
			FILE * savedCodebook,
			bool splitLargeClusters = false
			//---------------------------------------
			) :
			cache(cache),
			kmerLength(kmerLength),
			db(db),
			splitLargeClusters(splitLargeClusters),
			idx(db.Items())
			//---------------------------------------
		{
			int D = 0;

			//(cerr << "A" << endl).flush();

			Cluster * currentCluster = 0;

			const int BufferSize = 1000000;
			vector<char> _buffer(BufferSize);
			char * buffer = _buffer.data();
			size_t nextReadLoc = 0;
			size_t availSpace = BufferSize - 1;
			size_t bytesRead;

			vector<char *> lines;
			lines.reserve(BufferSize / 10);

			vector<pCluster> pendingClusters;
			vector<int> clusterFirstLine;
			vector<int> clusterLastLine;

			char * lastLineStart;
			bool codebookSeen = false;

			//(cerr << "B" << endl).flush();

			while ( 0 != (bytesRead = fread(buffer + nextReadLoc, sizeof(char), availSpace, savedCodebook)) ) {
				// Plug a trailing zero into the end of the buffer, just in case.
				buffer[BufferSize - 1] = 0;

				size_t max = bytesRead + nextReadLoc;
				size_t pos = 0;
				lines.clear();
				lastLineStart = 0;
				pendingClusters.clear();
				clusterFirstLine.clear();
				clusterLastLine.clear();

				if ( currentCluster ) {
					pendingClusters.push_back(currentCluster);
					clusterFirstLine.push_back(0);
					// clusterLastLine lags one behind, do not add anything at this point.
				}

				while ( pos < max ) {
					while ( pos < max && (buffer[pos] == '\n' || buffer[pos] == 0) ) {
						pos++;
					}

					lastLineStart = buffer + pos;

					while ( pos < max && !(buffer[pos] == '\n' || buffer[pos] == 0) ) {
						pos++;
					}

					if ( pos < max ) {
						buffer[pos++] = 0;
						lines.push_back(lastLineStart);
						lastLineStart = buffer + pos;
					}
				}

				//(cerr << "C" << endl).flush();

				for ( int i = 0; i < lines.size(); i++ ) {
					char *l = lines[i];

					if ( (!codebookSeen) && match(l, "Codebook") ) {
						codebookSeen = true;
						processCodebook(l);
					}
					else if ( match(l, "Cluster") ) {
						// Insert last line of previous cluster.
						if ( currentCluster ) clusterLastLine.push_back(i);

						// Create new cluster
						processCluster(l, currentCluster);

						// remember the new cluster
						pendingClusters.push_back(currentCluster);
						clusterFirstLine.push_back(i + 1);
					}
				}

				//(cerr << "D: " << D++ << endl).flush();

				// The trailing cluster will be incomplete, but it extends to the end of the buffer.
				clusterLastLine.push_back((int) lines.size());

				// #pragma omp parallel for
				for ( int c = 0; c < pendingClusters.size(); c++ ) {
					pCluster currentCluster = pendingClusters[c];

					for ( int i = clusterFirstLine[c]; i < clusterLastLine[c]; i++ ) {
						processLine(lines[i], currentCluster);
					}

					if ( c < pendingClusters.size() - 1 && pendingClusters[c]->items.size() != pendingClusters[c]->expectedSize ) {
						(cerr << "Cluster size is wrong!!!" << endl).flush();
					}
				}

				//(cerr << "E" << endl).flush();

				availSpace = lastLineStart - buffer;
				nextReadLoc = max - availSpace;
				memcpy(buffer, lastLineStart, nextReadLoc);
			}


			//(cerr << "F" << endl).flush();

			size_t kmerCount = 0;
			size_t kmerCountSquared = 0;

			for ( auto cluster : *this ) {
				auto x = cluster->Size();
				kmerCount += x;
				kmerCountSquared += x * x;
			}

			double mu = double(kmerCount) / codebook.size();
			double t = double(kmerCountSquared) / codebook.size();
			double sigma = sqrt(t - mu * mu);

			//(cerr << "G" << endl).flush();

			if ( splitLargeClusters ) {
				SplitLargeClusters(mu, sigma);
			}

			CopyKmerDataNonRecursive((int) Alphabet::WordsPerKmer(kmerLength, cache.CharsPerWord()));
			IdentifySharedKmers();

			//(cerr << "H" << endl).flush();

			cerr << codebook.size() << " clusters parsed, indexing " << kmerCount << " kmers." << endl;
		}

		~KmerCodebook() {
			for ( uint i = 0; i < codebook.size(); i++ ) {
				auto cluster = codebook[i];

				if ( cluster ) delete cluster;
			}
		}

		void IdentifySharedKmers() {
			auto less = [this](const Kmer & x, const Kmer & y) {
				return strncmp(x.String(), y.String(), kmerLength) < 0;
			};

			auto equal = [this](const Kmer & x, const Kmer & y) {
				return strncmp(x.String(), y.String(), kmerLength) == 0;
			};

#pragma omp parallel for
			for ( int c = 0; c < codebook.size(); c++ ) {
				vector<Kmer> &items = codebook[c]->items;

				if ( items.size() > 1 ) {
					sort(items.begin(), items.end(), less);

					items[0].equalsPredecessor = false;

					for ( int i = 1; i < items.size(); i++ ) {
						items[i].equalsPredecessor = equal(items[i], items[i - 1]);
					}
				}
			}
		}

		void SplitLargeClusters(double mu, double sigma) {
			int C = int(codebook.size());
			double limit = mu + 3 * sigma;

			for ( int i = 0; i < C; i++ ) {
				pCluster cluster = codebook[i];
				size_t cSize = cluster->Size();

				if ( cSize >= limit ) {
					int slices = int(ceil(cSize / mu));
					int itemsPerSlice = int(cSize / slices);

					for ( int j = 0; j < slices - 1; j++ ) {
						pCluster newCluster = new Cluster(cluster->sequence, cluster->kmerPosition, kmerLength, cluster->length);

						for ( int k = 0; k < itemsPerSlice; k++ ) {
							auto item = cluster->items.back();
							cluster->items.pop_back();
							newCluster->items.push_back(item);
						}

						newCluster->index = codebook.size();
						codebook.push_back(newCluster);
					}
				}
			}
		}

		typedef Cluster *pCluster;

		static bool match(const char * l, const char * s) {
			return (strncmp(l, s, strlen(s)) == 0);
		};

		// Advances the current character until a comma is reached.
		// Upon return, l[i] is a comma. If no comma is reached before
		// end of string, exception is thrown.
		static void gotoComma(const char * l, size_t & i) {
			while ( l[i] && l[i] != ',' ) i++;

			if ( l[i] != ',' ) throw Exception("Expected comma!", FileAndLine);
		};

		// Skips the current character, which is assumed to be a punctuation mark of some sort,
		// and parses an integer from the following positions. On return,
		// l[i] is the first non-digit encountered.
		static int parseInt(const char * l, size_t &i) {
			int y = 0;
			bool isNeg = l[i + 1] == '-';

			if ( isNeg ) i++;

			for ( i++; isdigit(l[i]); i++ ) {
				y = y * 10 + (l[i] - '0');
			}

			return isNeg ? -y : y;
		};

		void processCodebook(const char * l) {
			size_t i = 0;
			gotoComma(l, i);
			uint k = parseInt(l, i);

			if ( k != kmerLength ) {
				throw Exception("Invalid kmerLength", FileAndLine);
			}
		}

		void processLine(
			char * l,
			pCluster & currentCluster
			) {
			size_t i = 0;
			gotoComma(l, i);
			l[i] = 0;
			FastaSequence *seq = idx[l];
			uint offset = parseInt(l, i);
			currentCluster->Add(seq, offset, kmerLength);

			//(cerr << "Added " 
			//    << seq->Id() << ":" << offset 
			//    << " to cluster centred on " 
			//    << currentCluster->sequence->Id() << ":" << currentCluster->kmerPosition
			//    << endl).flush();
		}

		void processCluster(
			char * l,
			pCluster & currentCluster
			) {
			size_t i = 0;
			gotoComma(l, i);

			size_t expectedSize = parseInt(l, i);

			assert_equal(',', l[i]);
			i++;

			char * seqId = l + i;

			gotoComma(l, i);

			l[i] = 0;
			FastaSequence *seq = idx[seqId];

			uint offset = parseInt(l, i);

			//if ( idx == 33772 ) {
			//	(cerr << "l: " << l << endl).flush();
			//	(cerr << "offset: " << offset << endl).flush();
			//	(cerr << "expectedSize: " << expectedSize << endl).flush();
			//}

			currentCluster = new Cluster(seq, offset, kmerLength, expectedSize);
			currentCluster->index = codebook.size();
			codebook.push_back(currentCluster);
		}

		void CopyKmerData(int wordsPerKmer) {
			//(cerr << "G 1" << endl).flush();
			int N = (int) codebook.size();
			kmerData.resize(N, wordsPerKmer);

			//(cerr << "G 2" << endl).flush();
#pragma omp parallel for
			for ( int i = 0; i < N; i++ ) {
				//(cerr << "G 3a - " << i << endl).flush();
				//(cerr << "kmerData.row(i) = " << kmerData.row(i)  << endl).flush();
				//(cerr << "codebook[i] = " << codebook[i] << endl).flush();
				//(cerr << "codebook[i]->position = " << codebook[i]->position << endl).flush();
				//(cerr << "codebook[i] = " << codebook[i] << endl).flush();
				//(cerr << "codebook[i]->sequence = " << codebook[i]->sequence << endl).flush();
				//(cerr << "codebook[i]->sequence->Id() = " << codebook[i]->sequence->Id() << endl).flush();
				//(cerr << "codebook[i]->sequence->Sequence() = " << codebook[i]->sequence->Sequence() << endl).flush();
				//(cerr << "codebook[i]->Word() = " << codebook[i]->Word()  << endl).flush();
				//(cerr << "wordsPerKmer * sizeof(KmerWord) = " << (wordsPerKmer * sizeof(KmerWord))  << endl).flush();
				// (cerr << " = " <<   << endl).flush();

				memcpy(kmerData.row(i), codebook[i]->Word(), wordsPerKmer * sizeof(KmerWord));
				//(cerr << "G 3b - " << i << endl).flush();

				codebook[i]->CopyKmerData(wordsPerKmer);
				//(cerr << "G 3c - " << i << endl).flush();
			}

			//(cerr << "G 3" << endl).flush();
		}

		void CopyKmerDataNonRecursive(int wordsPerKmer) {
			auto N = codebook.size();
			kmerData.resize(N, wordsPerKmer);

#pragma omp parallel for
			for ( int i = 0; i < (int) N; i++ ) {
				memcpy(kmerData.row(i), codebook[i]->Word(), wordsPerKmer * sizeof(KmerWord));
			}
		}

		void AddWordsToClusters() {
			size_t n = db.Length();

			typedef FastaSequence *pFastaSequence;

			auto maxPos = max_element(db.Items().begin(), db.Items().end(),
				[](const pFastaSequence & x, const pFastaSequence & y) {
				return x->Sequence().length() < y->Sequence().length();
			});

			size_t maxLen = (*maxPos)->Sequence().length();

			vector<Cluster *> closest(maxLen - kmerLength + 1);

			for ( size_t i = 0; i < n; i++ ) {
				FastaSequence * seq = db[i];
				size_t len = seq->Sequence().length();
				int64_t kmerCount = len - kmerLength + 1;

#pragma omp parallel for
				for ( int64_t j = 0; j < kmerCount; j++ ) {
					Distance dist = numeric_limits<Distance>::max();
					Cluster * c = FindNearestCluster(seq, j, dist);
					closest[j] = c;
				}

				for ( uint j = 0; j < kmerCount; j++ ) {
					closest[j]->expectedSize++;
				}
			}

			for ( auto cluster : codebook ) {
				cluster->items.reserve(cluster->expectedSize);
			}

			for ( size_t i = 0; i < n; i++ ) {
				FastaSequence * seq = db[i];
				size_t len = seq->Sequence().length();
				int64_t kmerCount = len - kmerLength + 1;

#pragma omp parallel for 
				for ( int64_t j = 0; j < kmerCount; j++ ) {
					Distance dist = numeric_limits<Distance>::max();
					Cluster * c = FindNearestCluster(seq, j, dist);
					closest[j] = c;
				}

				for ( uint j = 0; j < kmerCount; j++ ) {
					closest[j]->Add(seq, j, kmerLength);
				}
			}
		}

		void AddWordsToClusters2() {
			size_t n = db.Length();

			typedef FastaSequence *pFastaSequence;

			size_t totalKmers = std::accumulate(db.Items().begin(), db.Items().end(), (size_t) 0,
				[this](size_t prev, const pFastaSequence & x) {
				return prev + x->Sequence().length() - kmerLength + 1;
			});

			vector<omp_lock_t> clusterLock(codebook_size);

#pragma omp parallel for
			for ( int c = 0; c < codebook_size; c++ ) {
				omp_init_lock(&clusterLock[c]);
				this->codebook[c]->items.reserve(totalKmers / codebook_size);
			}

			auto N = (int64_t) n;

#pragma omp parallel for schedule(dynamic,10)
			for ( int64_t i = 0; i < N; i++ ) {
				FastaSequence * seq = db[i];
				size_t len = seq->Sequence().length();
				int64_t kmerCount = len - kmerLength + 1;

				for ( int64_t j = 0; j < kmerCount; j++ ) {
					Distance dist = numeric_limits<Distance>::max();
					Cluster * closest = FindNearestCluster(seq, j, dist);

					omp_set_lock(&clusterLock[closest->index]);
					closest->expectedSize++;
					closest->Add(seq, j, kmerLength);
					omp_unset_lock(&clusterLock[closest->index]);
				}
			}

#pragma omp parallel for
			for ( int c = 0; c < codebook_size; c++ ) {
				omp_destroy_lock(&clusterLock[c]);
			}
		}

		vector<Cluster *> & Codebook() { return codebook; }

		Cluster * FindNearestCluster(FastaSequence * seq, size_t j, Distance & dist) const {
			Cluster * nearestCluster = codebook[0];
			KmerWord * seqStr = seq->GetEncodedKmer(j);

			dist = cache.GetDistance(seqStr, kmerData.row(0), kmerLength);

			for ( size_t i = 1; i < codebook_size; i++ ) {
				Distance d = cache.GetDistance(seqStr, kmerData.row(i), kmerLength);

				if ( d < dist ) {
					dist = d;
					nearestCluster = codebook[i];
				}
			}

			return nearestCluster;
		}

		ostream & Write(ostream & out) const {
			out << "Codebook," << kmerLength << "," << codebook_size << endl;
			for ( auto cluster : codebook ) {
				out << (*cluster);
			}
			return out;
		}

		vector<Cluster *>::iterator begin() {
			return codebook.begin();
		}

		vector<Cluster *>::iterator end() {
			return codebook.end();
		}

		vector<Cluster *>::reverse_iterator rbegin() {
			return codebook.rbegin();
		}

		vector<Cluster *>::reverse_iterator rend() {
			return codebook.rend();
		}
	};
}
