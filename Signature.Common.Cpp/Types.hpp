#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

namespace QutBio {
	typedef unsigned char byte;
	typedef unsigned int uint;
	typedef unsigned long ulong;

	template <typename T1, typename T2, typename T3> struct Triple {
		T1 item1;
		T2 item2;
		T3 item3;
	};
}
