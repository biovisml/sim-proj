#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <ios>

#include "FastaSequence.hpp"
#include "EncodedKmer.hpp"

using namespace std;

namespace QutBio {
	struct Kmer {
		FastaSequence * sequence;
		size_t kmerPosition;
		size_t length;
		bool equalsPredecessor = false;

		Kmer() :
			sequence(0), kmerPosition(0), length(0) {}

		Kmer(FastaSequence * seq, size_t kmerPosition, size_t length) :
			sequence(seq), kmerPosition(kmerPosition), length(length) {}

		const char * String() const {
			return sequence->Sequence().c_str() + kmerPosition;
		}

		const EncodedKmer Word() const {
			return sequence->GetEncodedKmer(kmerPosition);
		}

		friend ostream & operator << (ostream & str, const Kmer & kmer ) {
			str << kmer.sequence->Id() << "," << kmer.kmerPosition;
			return str;
		}

		Kmer & operator=(const Kmer & other) {
			this->sequence = other.sequence;
			this->kmerPosition = other.kmerPosition;
			this->length = other.length;

			return *this;
		}

		bool operator==(const Kmer & other) const {
			const char *x = this->String();
			const char *y = other.String();

			for ( size_t i = 0; i < length; i++ ) {
				if ( x[i] != y[i] ) {
					return false;
				}
			}

			return true;
		}

		bool operator<(const Kmer & other) const {
			const char *x = this->String();
			const char *y = other.String();

			for ( size_t i = 0; i < length; i++ ) {
				if ( x[i] > y[i] ) {
					return false;
				}
				else if ( x[i] < y[i] ) {
					return true;
				}
			}

			return false;
		}

		/**
		**	Gets a Kmer that sorts less than all other kmers of the same length.
		**	Zeros of every length are attached to a common, zero-filled string.
		*/
		static Kmer Zero( size_t k ) {
			static string s;
			 
			while ( s.size() < k ) {
				s.push_back('\0');
			}

			static FastaSequence zero("zero", "", "", s);
			Kmer val( &zero, 0, k);
			return val;
		}

		ostream & write(ostream & stream) const {
			const char * str = String();

			for ( int i = 0; i < length; i++ ) {
				stream.put(str[i]);
			}

			return stream;
		}
	};


}

