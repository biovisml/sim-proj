#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include "Alphabet.hpp"
#include "SimilarityMatrix.hpp"
#include "EncodedKmer.hpp"
#include "FastaSequence.hpp"
#include "FragDistProcessor.hpp"
#include "Fragment.hpp"
#include "FreeList.hpp"
#include "SequenceDistanceFunction.hpp"
#include "Util.hpp"
#include "DiagonalGenerator.hpp"

#include <string>
#include <ctime>
#include <cfloat>
using namespace std;

#undef max
#undef min

namespace QutBio {

	class HausdorffCalculator : SequenceDistanceFunction {
	protected:
		/// <summary>
		///		kmer distance cache with max(queryKmerCount) rows and max(subjectKmerCount) columns.
		///	</summary>
		Distance **kmerDistCache = 0;
		vector<Distance> rowMinima;
		vector<Distance> colMinima;
		FragmentAggregationMode * kmerMode;
		FragmentAggregationMode * fragMode;
		uint fragmentLength;
		uint kmerLength;
		size_t maxQueryLength;
		size_t maxSubjectLength;

#if defined(USE_BIG_CACHE)
		int vocabSize;
		size_t charsPerWord;
		KmerDistanceCache & cachedCalculator;
		KmerDistanceFunction kmerCodeDistance;
#endif // defined(USE_BIG_CACHE)

		typedef Distance(*FragmentDistance)(
			HausdorffCalculator & This,

			const char * queryChars,
			size_t qStart,
			size_t qEnd,

			const char * subjectChars,
			size_t sStart,
			size_t sEnd
			);

		FragmentDistance fragmentDistance;

		typedef double(*CollectionDistance) (
			HausdorffCalculator & This,

			const char * queryChars,
			size_t queryKmerCount,
			size_t queryFragCount,

			const char * subjectChars,
			size_t subjectKmerCount,
			size_t subjectFragCount
			);

		CollectionDistance collectionDistance;

		DiagonalGenerator::DistanceProcessor processDistance;

		Distance lookup[128][128];

		Distance thresholdDistance = BAD_DIST;
		Distance defaultDistance = BAD_DIST;

		bool pushKmerDistances;
	public:
		HausdorffCalculator(
			SimilarityMatrix * matrix,
			uint kmerLength,
			FragmentAggregationMode * kmerMode,
			FragmentAggregationMode * fragMode,
			Alphabet * alphabet,
			uint fragLength,
			size_t maxQueryLength,
			size_t maxSubjectLength,
			bool pushKmerDistances
			) :
			SequenceDistanceFunction(matrix, kmerLength),
			fragmentLength(fragLength),
			kmerLength(kmerLength),
			kmerMode(kmerMode),
			fragMode(fragMode),
			maxQueryLength(maxQueryLength),
			maxSubjectLength(maxSubjectLength),
			pushKmerDistances(pushKmerDistances) {

			FragmentDistance fragmentDistances[] = {
				KmerBestOfBest,
				KmerHausdorff,
				KmerHausdorffAverage,
				KmerHausdorffAverageAverage,
				KmerSlice,
				KmerSliceVertical,
				KmerSliceNoFollow,
				KmerSliceVerticalNoFollow,
			};

			fragmentDistance = fragmentDistances[kmerMode->Value()];

			CollectionDistance collectionDistances[] = {
				FragBestOfBest,
				FragHausdorff,
				FragHausdorffAverage,
				pushKmerDistances ? FragHausdorffAverageAverage_Next : FragHausdorffAverageAverage
			};

			collectionDistance = collectionDistances[fragMode->Value()];

#pragma region A transitional arrangement while I implement the new distance-pushing architecture.

			DiagonalGenerator::DistanceProcessor distanceProcessors[] = {
				ProcessDistance,
				0, // KmerHausdorff,
				0, // KmerHausdorffAverage,
				0, // KmerHausdorffAverageAverage,
				0, // KmerSlice,
				0, // KmerSliceVertical,
				0, // KmerSliceNoFollow,
				0, // KmerSliceVerticalNoFollow,
			};

			processDistance = distanceProcessors[kmerMode->Value()];

#pragma endregion

			PreallocateKmerDistCache();

			for ( int i = 0; i < 128; i++ ) {
				for ( int j = 0; j < 128; j++ ) {
					lookup[i][j] = matrix->maxValue - matrix->dict[i][j];
				}
			}
		}

		virtual ~HausdorffCalculator() {
			for ( size_t i = 0; i < maxQueryLength; i++ ) {
				delete[] kmerDistCache[i];
			}
			delete[] kmerDistCache;
		}

		static void ProcessDistance(
			void * processingObject,
			size_t queryPos,
			size_t subjectPos,
			Distance distance
			) {
			FragDistProcessor<vector<Distance>, Distance> *processor = (FragDistProcessor<vector<Distance>, Distance> *) processingObject;
			processor->ProcessDistance(queryPos, subjectPos, distance);
		}

		void SetThreshold(Distance thresholdDistance, Distance defaultDistance) {
			assert_false(IS_BAD_DIST(thresholdDistance));
			assert_false(IS_BAD_DIST(defaultDistance));
			assert_true(thresholdDistance <= defaultDistance);

			this->thresholdDistance = thresholdDistance;
			this->defaultDistance = defaultDistance;
		}

		double ThresholdDistance() { return thresholdDistance; }

		double DefaultDistance() { return defaultDistance; }

		double ComputeDistance(
			FastaSequence * querySeq,
			FastaSequence * subjectSeq
			) {
			auto &queryStr = querySeq->Sequence();
			size_t queryKmerCount = queryStr.size() - kmerLength + 1;

			auto & subjectStr = subjectSeq->Sequence();
			size_t subjectKmerCount = subjectStr.size() - kmerLength + 1;

			const size_t queryFragCount = Fragment::GetCount(queryKmerCount, fragmentLength);
			const size_t subjectFragCount = Fragment::GetCount(subjectKmerCount, fragmentLength);

			// fragDistCache.clear();

			if ( pushKmerDistances ) {
				// messy, but only some functions are implemented in the push scenario.
				UpdateKmerDistCache_Next(
					querySeq, queryStr.c_str(), queryKmerCount, queryFragCount,
					subjectSeq, subjectStr.c_str(), subjectKmerCount, subjectFragCount
					);
			}
			else {
				UpdateKmerDistCache(queryStr.c_str(), subjectStr.c_str(), queryKmerCount, subjectKmerCount);
			}

			double distance = collectionDistance(
				*this,
				queryStr.c_str(), queryKmerCount, queryFragCount,
				subjectStr.c_str(), subjectKmerCount, subjectFragCount
				);

			return distance;
		}

	private:

		/// <summary>
		///	Given strings s and t, having length m and n respectively, and a similarity matrix 
		///	reformatted as a 128 by 128 array of integers, populates a vector contain a kmer mutual
		///	distance table.
		/// </summary>
		///	<param name="queryChars"></param>

		void UpdateKmerDistCache_Next(
			FastaSequence * querySeq,
			const char * queryChars,
			size_t queryKmerCount,
			size_t queryFragCount,

			FastaSequence * subjectSeq,
			const char * subjectChars,
			size_t subjectKmerCount,
			size_t subjectFragCount
			) {

			// TODO: turn this into a program argument.
			const int fragsPerTile = 1;

			const SimilarityMatrix * matrix = this->matrix;
			const Distance max = matrix->MaxValue();

			assert_true(queryFragCount > 0 && subjectFragCount > 0);

			double qStepSize = Fragment::GetRealStepSize(queryKmerCount, fragmentLength, queryFragCount);
			vector<size_t> queryFragMapping(queryKmerCount);
			FragDistProcessor_Frag<vector<Distance>, Distance>::GetFragmentMapping(queryFragMapping, queryKmerCount, queryFragCount, qStepSize);

			double sStepSize = Fragment::GetRealStepSize(subjectKmerCount, fragmentLength, subjectFragCount);
			vector<size_t> subjectFragMapping(subjectKmerCount);
			FragDistProcessor_Frag<vector<Distance>, Distance>::GetFragmentMapping(subjectFragMapping, subjectKmerCount, subjectFragCount, sStepSize);

			rowMinima.resize(queryFragCount, defaultDistance);
			colMinima.resize(subjectFragCount, defaultDistance);

			FragDistProcessor_Frag<vector<Distance>, Distance> data(
				rowMinima,
				colMinima,
				queryKmerCount,
				subjectKmerCount,
				0,
				queryFragCount,
				subjectFragCount,
				fragmentLength,
				fragsPerTile,
				queryFragMapping.data(),
				subjectFragMapping.data()
				);

			DiagonalGenerator generator;
			generator.GenerateDistances(querySeq, subjectSeq, queryChars, subjectChars, kmerLength, queryKmerCount, subjectKmerCount, matrix, &data, processDistance);
		}

		/// <summary>
		///	Given strings s and t, having length m and n respectively, and a similarity matrix 
		///	reformatted as a 128 by 128 array of integers, populates a vector contain a kmer mutual
		///	distance table.
		/// </summary>
		///	<param name="queryChars"></param>

		void UpdateKmerDistCache(
			const char * queryChars,
			const char * subjectChars,
			size_t queryKmerCount,
			size_t subjectKmerCount
			) {
			const SimilarityMatrix * matrix = this->matrix;
			const Distance max = matrix->MaxValue();
			const size_t m = queryKmerCount;
			const size_t n = subjectKmerCount;

			for ( int r = 0; r < (int) m; r++ ) {
				const int c_upper = r == 0 ? (int) n : 1;

				//	Do the top-right part of the rectangle
				for ( int c = 0; c < c_upper; c++ ) {
					Distance buffer[1000];
					const char * a = subjectChars + c;
					const char * b = queryChars + r;
					Distance distance = 0;

					size_t diagLength = std::min(m - r, n - c);

					// Prime the circular buffer with the first kmer in the query
					for ( size_t t = 0; t < kmerLength; t++, a++, b++ ) {
						Distance currentTerm = lookup[*a][*b];
						distance += currentTerm;
						buffer[t] = currentTerm;
					}

					kmerDistCache[r][c] = distance;

					for ( size_t offset = 1, buffptr = 0;
						offset < diagLength;
						a++, b++, offset++, buffptr++
						) {
						if ( buffptr >= kmerLength ) {
							buffptr = 0;
						}

						distance -= buffer[buffptr];
						Distance currentTerm = lookup[*a][*b];
						buffer[buffptr] = currentTerm;
						distance += currentTerm;

						kmerDistCache[r + offset][c + offset] = distance;
					}
				}
			}
		}

	protected:

		void PreallocateKmerDistCache() {

#if defined(USE_BIG_CACHE)
			queryTiles.Resize(maxQueryLength);
			subjectTiles.Resize(maxSubjectLength);
#endif // defined(USE_BIG_CACHE)

			kmerDistCache = new pDistance[maxQueryLength];

			for ( size_t i = 0; i < maxQueryLength; i++ ) {
				kmerDistCache[i] = new Distance[maxSubjectLength];
			}

			size_t maxQueryFragCount = Fragment::GetCount(maxQueryLength, fragmentLength);
			size_t maxSubjectFragCount = Fragment::GetCount(maxSubjectLength, fragmentLength);

			rowMinima.resize(maxQueryFragCount),
				colMinima.resize(maxSubjectFragCount);
		}

		static uint GetMaxLength(const vector<FastaSequence *> & dataset) {
			uint queryMax = 0;

			for ( uint i = 0; i < dataset.size(); i++ ) {
				uint len = (uint) dataset[i]->Sequence().size();

				if ( len > queryMax ) {
					queryMax = len;
				}
			}

			return queryMax;
		}

	public:

		///	<summary>
		///		Calculates the threshold Hausdorff average (bidirectional) 
		///		distance between two fragments using the maximum of the two
		///		one way distances.
		///	</summary>
		static double FragHausdorffAverage(
			HausdorffCalculator & This,

			const char * queryChars,
			size_t queryKmerCount,
			size_t queryFragCount,

			const char * subjectChars,
			size_t subjectKmerCount,
			size_t subjectFragCount
			) {
			double totXY = 0;
			int obsXY = 0;

			double totYX = 0;
			int obsYX = 0;

			double qStepSize = Fragment::GetRealStepSize(queryKmerCount, This.fragmentLength, queryFragCount);
			double sStepSize = Fragment::GetRealStepSize(subjectKmerCount, This.fragmentLength, subjectFragCount);

			// Compute one-way distance from x to y, and also cache distances.

			uint qStart = 0;

			for ( uint i = 0; i < queryFragCount; i++ ) {
				uint qEnd = Fragment::GetFragmentStart(i + 1, qStepSize, queryKmerCount);

				double min = DBL_MAX;
				uint minIdx = numeric_limits<uint>::max();

				uint sStart = 0;

				for ( uint j = 0; j < subjectFragCount; j++ ) {
					uint sEnd = Fragment::GetFragmentStart(j + 1, sStepSize, subjectKmerCount);

					auto distance = This.fragmentDistance(
						This,
						queryChars, qStart, qEnd,
						subjectChars, sStart, sEnd
						);

					if ( distance < This.rowMinima[i] ) This.rowMinima[i] = distance;
					if ( distance < This.colMinima[j] ) This.colMinima[j] = distance;

					sStart = sEnd;
				}

				qStart = qEnd;
				obsXY++;
				totXY += This.rowMinima[i];
			}

			// Compute one-way distance from y to x, using cache distances.
			for ( uint j = 0; j < subjectFragCount; j++ ) {
				obsYX++;
				totYX += This.colMinima[j];
			}

			double avgXY = totXY / obsXY;
			double avgYX = totYX / obsYX;
			double result = avgXY > avgYX ? avgXY : avgYX;
			return result;
		}

		///	<summary>
		///		Calculates the threshold Hausdorff average (bidirectional) 
		///		distance between two fragments using the average of the two 
		///		one-way fragment distances.
		///	</summary>
		static double FragHausdorffAverageAverage(
			HausdorffCalculator & This,

			const char * queryChars,
			size_t queryKmerCount,
			size_t queryFragCount,

			const char * subjectChars,
			size_t subjectKmerCount,
			size_t subjectFragCount
			) {
			double totXY = 0;
			int obsXY = 0;

			double totYX = 0;
			int obsYX = 0;


			double qStepSize = Fragment::GetRealStepSize(queryKmerCount, This.fragmentLength, queryFragCount);
			double sStepSize = Fragment::GetRealStepSize(subjectKmerCount, This.fragmentLength, subjectFragCount);

			// Compute one-way distance from x to y, and also cache distances.

			uint qStart = 0;
			for ( uint i = 0; i < queryFragCount; i++ ) {
				uint qEnd = Fragment::GetFragmentStart(i + 1, qStepSize, queryKmerCount);
				uint sStart = 0;

				// // #pragma omp parallel for
				for ( int j = 0; j < (int) subjectFragCount; j++ ) {
					uint sEnd = Fragment::GetFragmentStart(j + 1, sStepSize, subjectKmerCount);

					auto distance = This.fragmentDistance(
						This,
						queryChars, qStart, qEnd,
						subjectChars, sStart, sEnd
						);

					if ( distance < This.rowMinima[i] ) This.rowMinima[i] = distance;
					if ( distance < This.colMinima[j] ) This.colMinima[j] = distance;

					sStart = sEnd;
				}

				qStart = qEnd;
				obsXY++;
				totXY += This.rowMinima[i];
			}

			// Compute one-way distance from y to x, using cache distances.
			for ( uint j = 0; j < subjectFragCount; j++ ) {
				obsYX++;
				totYX += This.colMinima[j];
			}

			double avgXY = totXY / obsXY;
			double avgYX = totYX / obsYX;

			double result = (avgXY + avgYX) / 2;
			return result;
		}

		///	<summary>
		///		Calculates the threshold Hausdorff average (bidirectional) 
		///		distance between two fragments using the average of the two 
		///		one-way fragment distances.
		///	<para>
		///		This function assumes that the fragment distance cache has already been 
		///		populated.
		///	</summary>
		static double FragHausdorffAverageAverage_Next(
			HausdorffCalculator & This,

			const char * queryChars_notUsed,
			size_t queryKmerCount_notUsed,
			size_t queryFragCount,

			const char * subjectChars_notUsed,
			size_t subjectKmerCount_notUsed,
			size_t subjectFragCount
			) {

			return FragHausdorffAverageAverage_FragDistCache(This.rowMinima, This.colMinima, queryFragCount, subjectFragCount);
		}

		///	<summary>
		///		Calculates the threshold Hausdorff average (bidirectional) 
		///		distance between two fragments using the average of the two 
		///		one-way fragment distances.
		///	<para>
		///		This function assumes that the fragment distance cache has already been 
		///		populated.
		///	</summary>
		static double FragHausdorffAverageAverage_FragDistCache(
			vector<Distance> & rowMinima,
			vector<Distance> & colMinima,
			size_t queryFragCount,
			size_t subjectFragCount
			) {
			int totXY = 0;
			int totYX = 0;

			// Compute one-way distance from x to y, and also cache distances.
			for ( uint i = 0; i < queryFragCount; i++ ) {
				totXY += rowMinima[i];
			}

			// Compute one-way distance from y to x, using cache distances.
			for ( uint j = 0; j < subjectFragCount; j++ ) {
				totYX += colMinima[j];
			}

			double avgXY = (double) totXY / queryFragCount;
			double avgYX = (double) totYX / subjectFragCount;

			double result = (avgXY + avgYX) / 2;
			return result;
		}

		static double FragHausdorff(
			HausdorffCalculator & This,

			const char * queryChars,
			size_t queryKmerCount,
			size_t queryFragCount,

			const char * subjectChars,
			size_t subjectKmerCount,
			size_t subjectFragCount
			) {
			double maxXY = -DBL_MAX;
			double maxYX = -DBL_MAX;

			double qStepSize = Fragment::GetRealStepSize(queryKmerCount, This.fragmentLength, queryFragCount);
			double sStepSize = Fragment::GetRealStepSize(subjectKmerCount, This.fragmentLength, subjectFragCount);

			// Compute one-way distance from x to y, and also cache distances.

			uint qStart = 0;

			for ( int i = 0; i < (int) queryFragCount; i++ ) {
				uint qEnd = Fragment::GetFragmentStart(i + 1, qStepSize, queryKmerCount);

				uint sStart = 0;

				for ( int j = 0; j < (int) subjectFragCount; j++ ) {
					uint sEnd = Fragment::GetFragmentStart(j + 1, sStepSize, subjectKmerCount);

					auto distance = This.fragmentDistance(This, queryChars, qStart, qEnd, subjectChars, sStart, sEnd);

					if ( distance < This.rowMinima[i] ) This.rowMinima[i] = distance;
					if ( distance < This.colMinima[j] ) This.colMinima[j] = distance;

					sStart = sEnd;
				}

				qStart = qEnd;
			}

			// Compute one-way distance from x to y, and also cache distances.
			for ( uint i = 0; i < queryFragCount; i++ ) {
				if ( This.rowMinima[i] > maxXY ) {
					maxXY = This.rowMinima[i];
				}
			}

			// Compute one-way distance from y to x, using cache distances.
			for ( uint j = 0; j < subjectFragCount; j++ ) {
				if ( This.colMinima[j] > maxYX ) {
					maxYX = This.colMinima[j];
				}
			}

			return maxXY > maxYX ? maxXY : maxYX;
		}

		static double FragBestOfBest(
			HausdorffCalculator & This,

			const char * queryChars,
			size_t queryKmerCount,
			size_t queryFragCount,

			const char * subjectChars,
			size_t subjectKmerCount,
			size_t subjectFragCount
			) {
			double minDist = numeric_limits<double>::max();

			double qStepSize = Fragment::GetRealStepSize(queryKmerCount, This.fragmentLength, queryFragCount);
			double sStepSize = Fragment::GetRealStepSize(subjectKmerCount, This.fragmentLength, subjectFragCount);

			// Compute one-way distance from x to y, and also cache distances.

			uint qStart = 0;

			for ( uint i = 0; i < queryFragCount; i++ ) {
				uint qEnd = Fragment::GetFragmentStart(i + 1, qStepSize, queryKmerCount);

				uint sStart = 0;

				for ( uint j = 0; j < subjectFragCount; j++ ) {
					uint sEnd = Fragment::GetFragmentStart(j + 1, sStepSize, subjectKmerCount);

					auto distance = This.fragmentDistance(This, queryChars, qStart, qEnd, subjectChars, sStart, sEnd);

					if ( distance < This.rowMinima[i] ) This.rowMinima[i] = distance;
					if ( distance < This.colMinima[j] ) This.colMinima[j] = distance;

					sStart = sEnd;
				}

				qStart = qEnd;
			}

			for ( uint i = 0; i < queryFragCount; i++ ) {
				if ( This.rowMinima[i] < minDist ) {
					minDist = This.rowMinima[i];
				}
			}

			return minDist;
		}

		/// <summary> Returns the min-of-min kmer distance between two sets of encoded kmers,
		/// </summary>
		/// <param name="qStart">The index of the first kmer in the query fragment.</param>
		/// <param name="qEnd">The index of the first kmer AFTER the end of the query fragment.</param>
		/// <param name="sStart">The index of the first kmer in the reference fragment.</param>
		/// <param name="sEnd">The index of the first kmer AFTER the end of the reference fragment.</param>
		/// <param name="len">The number of kmers in the fragment.</param>
		///	<param name="threshold">Early stopping criterion for summative, non-negative, distances. 
		///		If cumulative distance equals or exceeds threshold, stop calculating and return cumulative 
		///		value at the stopping time.
		///	</param>
		/// <returns></returns>

		static Distance KmerBestOfBest(
			HausdorffCalculator & This,

			const char * queryChars,
			size_t qStart,
			size_t qEnd,

			const char * subjectChars,
			size_t sStart,
			size_t sEnd
			) {
			Distance minDist = numeric_limits<Distance>::max();

			for ( size_t i = qStart; i < qEnd; i++ ) {
				pDistance row = This.kmerDistCache[i];

				for ( size_t j = sStart; j < sEnd; j++ ) {
					Distance distance = row[j];

					if ( distance < minDist ) {
						minDist = distance;
					}
				}
			}

			if ( !IS_BAD_DIST(This.thresholdDistance) ) {
				return minDist <= This.thresholdDistance ? minDist : This.defaultDistance;
			}
			else {
				return minDist;
			}
		}

		static Distance KmerSlice(
			HausdorffCalculator & This,

			const char * queryChars,
			size_t qStart,
			size_t qEnd,

			const char * subjectChars,
			size_t sStart,
			size_t sEnd
			) {
			Distance minDist = numeric_limits<Distance>::max();
			size_t minI = 0;
			size_t minJ = 0;

			for ( size_t i = qStart; i < qEnd; i++ ) {
				size_t j0 = (sEnd - 1) - (i - qStart) * (sEnd - sStart) / (qEnd - qStart);

				for ( int j = (int) j0; j >= (int) sStart && j >= (int) j0 - 1; j-- ) {
					// assert_true((int) sStart <= j && j < (int) sEnd);
					Distance distance = This.matrix->Difference(queryChars + i, subjectChars + j, This.kmerLength);

					if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
						distance = This.defaultDistance;
					}

					if ( distance < minDist ) {
						minDist = distance;
						minI = i;
						minJ = j;
					}
				}
			}

			for ( size_t i = minI + 1, j = minJ + 1; i < qEnd && j < sEnd; i++, j++ ) {
				Distance distance = This.matrix->Difference(queryChars + i, subjectChars + j, This.kmerLength);

				if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
					distance = This.defaultDistance;
				}

				if ( distance < minDist ) {
					minDist = distance;
				}
			}

			for ( int i = (int) minI - 1, j = (int) minJ - 1; i >= (int) qStart && j >= (int) sStart; i--, j-- ) {
				Distance distance = This.matrix->Difference(queryChars + i, subjectChars + j, This.kmerLength);

				if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
					distance = This.defaultDistance;
				}

				if ( distance < minDist ) {
					minDist = distance;
				}
			}

			return minDist;
		}

		static Distance KmerSliceVertical(
			HausdorffCalculator & This,

			const char * queryChars,
			size_t qStart,
			size_t qEnd,

			const char * subjectChars,
			size_t sStart,
			size_t sEnd
			) {
			Distance minDist = numeric_limits<Distance>::max();;
			size_t minI = 0;
			size_t minJ = 0;

			for ( size_t i = qStart; i < qEnd; i++ ) {
				size_t j = sStart + (sEnd - sStart) / 2;

				Distance distance = This.matrix->Difference(queryChars + i, subjectChars + j, This.kmerLength);

				if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
					distance = This.defaultDistance;
				}

				if ( distance < minDist ) {
					minDist = distance;
					minI = i;
					minJ = j;
				}
			}

			for ( size_t i = minI + 1, j = minJ + 1; i < qEnd && j < sEnd; i++, j++ ) {
				Distance distance = This.matrix->Difference(queryChars + i, subjectChars + j, This.kmerLength);

				if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
					distance = This.defaultDistance;
				}

				if ( distance < minDist ) {
					minDist = distance;
				}
			}

			for ( int i = (int) minI - 1, j = (int) minJ - 1; i >= (int) qStart && j >= (int) sStart; i--, j-- ) {
				Distance distance = This.matrix->Difference(queryChars + i, subjectChars + j, This.kmerLength);

				if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
					distance = This.defaultDistance;
				}

				if ( distance < minDist ) {
					minDist = distance;
				}
			}

			return minDist;
		}

		static Distance KmerSliceNoFollow(
			HausdorffCalculator & This,

			const char * queryChars,
			size_t queryKmerCount,
			size_t qStart,

			const char * subjectChars,
			size_t subjectKmerCount,
			size_t sStart
			) {
			Distance minDist = numeric_limits<Distance>::max();
			size_t qEnd = min(qStart + This.fragmentLength, queryKmerCount);
			size_t sEnd = min(sStart + This.fragmentLength, subjectKmerCount);

			for ( size_t i = qStart; i < qEnd; i++ ) {
				size_t j0 = (sEnd - 1) - (i - qStart) * (sEnd - sStart) / (qEnd - qStart);

				for ( int j = (int) j0; j >= (int) sStart && j >= (int) j0 - 1; j-- ) {
					assert_true((int) sStart <= j && j < (int) sEnd);

					Distance distance = This.matrix->Difference(queryChars + i, subjectChars + j, This.kmerLength);

					if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
						distance = This.defaultDistance;
					}

					if ( distance < minDist ) {
						minDist = distance;
					}
				}
			}

			return minDist;
		}

		static Distance KmerSliceVerticalNoFollow(
			HausdorffCalculator & This,

			const char * queryChars,
			size_t queryKmerCount,
			size_t qStart,

			const char * subjectChars,
			size_t subjectKmerCount,
			size_t sStart
			) {
			Distance minDist = numeric_limits<Distance>::max();
			size_t qEnd = min(qStart + This.fragmentLength, queryKmerCount);
			size_t sEnd = min(sStart + This.fragmentLength, subjectKmerCount);

			for ( size_t i = qStart; i < qEnd; i++ ) {
				size_t j = sStart + (sEnd - sStart) / 2;

				Distance distance = This.matrix->Difference(queryChars + i, subjectChars + j, This.kmerLength);

				if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
					distance = This.defaultDistance;
				}

				if ( distance < minDist ) {
					minDist = distance;
				}
			}

			return minDist;
		}

		/// <summary> Returns the max(max-of-min) kmer distance between two sets of encoded kmers,
		/// </summary>
		/// <param name="qStart">The index of the first kmer in the query fragment.</param>
		///	<param name="qEnd">The index of the first kmer AFTER the end of the query fragment.</param>
		/// <param name="sStart">The index of the first kmer in the reference fragment.</param>
		///	<param name="sEnd">The index of the first kmer AFTER the end of the reference fragment.</param>
		/// <param name="len">The number of kmers in the fragment.</param>
		///	<param name="threshold">Early stopping criterion for summative, non-negative, distances. 
		///		If cumulative distance equals or exceeds threshold, stop calculating and return cumulative 
		///		value at the stopping time.
		///	</param>
		/// <returns></returns>

		static Distance KmerHausdorff(
			HausdorffCalculator & This,

			const char * queryChars,
			size_t qStart,
			size_t qEnd,

			const char * subjectChars,
			size_t sStart,
			size_t sEnd
			) {
			Distance maxXY = numeric_limits<short>::min();
			Distance maxYX = numeric_limits<short>::min();

			// Compute one-way distance from x to y, and also cache distances.
			for ( size_t i = qStart; i < qEnd; i++ ) {
				Distance min = numeric_limits<Distance>::max();
				pDistance row = This.kmerDistCache[i];

				for ( size_t j = sStart; j < sEnd; j++ ) {
					Distance distance = row[j];

					if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
						distance = This.defaultDistance;
					}

					if ( distance < min ) {
						min = distance;
					}
				}

				if ( min > maxXY ) {
					maxXY = min;
				}
			}

			// Compute one-way distance from y to x, using cache distances.
			for ( size_t j = sStart; j < sEnd; j++ ) {
				Distance min = numeric_limits<Distance>::max();

				for ( size_t i = qStart; i < qEnd; i++ ) {
					Distance distance = This.kmerDistCache[i][j];

					if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
						distance = This.defaultDistance;
					}

					if ( distance < min ) {
						min = distance;
					}
				}

				if ( min > maxYX ) {
					maxYX = min;
				}
			}

			return maxXY > maxYX ? maxXY : maxYX;
		}

		/// <summary> Returns the max(average-of-min) kmer distance between two sets of encoded kmers,
		/// </summary>
		/// <param name="qStart">The index of the first kmer in the query fragment.</param>
		/// <param name="qEnd">The index of the first kmer AFTER the end of the query fragment.</param>
		/// <param name="sStart">The index of the first kmer in the reference fragment.</param>
		/// <param name="sEnd">The index of the first kmer AFTER the end of the reference fragment.</param>
		/// <param name="len">The number of kmers in the fragment.</param>
		/// <returns></returns>

		static Distance KmerHausdorffAverage(
			HausdorffCalculator & This,

			const char * queryChars,
			size_t qStart,
			size_t qEnd,

			const char * subjectChars,
			size_t sStart,
			size_t sEnd
			) {
			int totXY = 0;
			int totYX = 0;

			// Compute one-way distance from x to y, using pre-cached cache distances.
			for ( size_t i = qStart; i < qEnd; i++ ) {
				Distance min = numeric_limits<Distance>::max();
				pDistance row = This.kmerDistCache[i];

				for ( size_t j = sStart; j < sEnd; j++ ) {
					Distance distance = row[j];

					if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
						distance = This.defaultDistance;
					}

					if ( distance < min ) {
						min = distance;
					}
				}

				totXY += min;
			}

			// Compute one-way distance from y to x, using cache distances.
			for ( size_t j = sStart; j < sEnd; j++ ) {
				Distance min = numeric_limits<Distance>::max();

				for ( size_t i = qStart; i < qEnd; i++ ) {
					Distance distance = This.kmerDistCache[i][j];

					if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
						distance = This.defaultDistance;
					}

					if ( distance < min ) {
						min = distance;
					}
				}

				totYX += min;
			}

			double avgXY = (double) totXY / (qEnd - qStart);
			double avgYX = (double) totYX / (sEnd - sStart);

			return avgXY > avgYX ? (Distance) round(avgXY) : (Distance) round(avgYX);
		}

		/// <summary> Returns the average(average-of-min) kmer distance between two sets of encoded kmers,
		/// </summary>
		/// <param name="qStart">The index of the first kmer in the query fragment.</param>
		/// <param name="qEnd">The index of the first kmer AFTER the end of the query fragment.</param>
		/// <param name="sStart">The index of the first kmer in the reference fragment.</param>
		/// <param name="sEnd">The index of the first kmer AFTER the end of the reference fragment.</param>
		/// <returns></returns>

		static Distance KmerHausdorffAverageAverage(
			HausdorffCalculator & This,

			const char * queryChars,
			size_t qStart,
			size_t qEnd,

			const char * subjectChars,
			size_t sStart,
			size_t sEnd
			) {
			int totXY = 0;
			int totYX = 0;

			// Compute one-way distance from x to y, using pre-cached cache distances.
			for ( size_t i = qStart; i < qEnd; i++ ) {
				Distance min = numeric_limits<Distance>::max();
				pDistance row = This.kmerDistCache[i];

				for ( size_t j = sStart; j < sEnd; j++ ) {
					Distance distance = row[j];

					if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
						distance = This.defaultDistance;
					}

					if ( distance < min ) {
						min = distance;
					}
				}

				totXY += min;
			}

			// Compute one-way distance from y to x, using cache distances.
			for ( size_t j = sStart; j < sEnd; j++ ) {
				Distance min = numeric_limits<Distance>::max();

				for ( size_t i = qStart; i < qEnd; i++ ) {
					Distance distance = This.kmerDistCache[i][j];

					if ( !IS_BAD_DIST(This.thresholdDistance) && distance > This.thresholdDistance ) {
						distance = This.defaultDistance;
					}

					if ( distance < min ) {
						min = distance;
					}
				}

				totYX += min;
			}

			double avgXY = (double) totXY / (qEnd - qStart);
			double avgYX = (double) totYX / (sEnd - sStart);

			return (Distance) round((avgXY + avgYX) / 2);
		}

	};
}
