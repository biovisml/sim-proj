#pragma once

// Trick Visual studio.
#if __cplusplus < 201103L
#undef __cplusplus
#define __cplusplus 201103L
#endif

#include <algorithm>
#include <cmath>
#include <functional>
#include <ios>
#include <vector>

using namespace std;

#include "Assert.hpp"
#include "CsvIO.hpp"
#include "Distribution.hpp"
#include "Exception.hpp"

namespace QutBio {
	class ImpossibleDistribution : public Distribution {
	public:
		virtual double Cdf(double t) { return 0; }
		virtual double Pdf(double t) { return 0; }
		virtual double InverseCdf(double t) { return NAN; }
		virtual double Mean(void) { return NAN; }
		virtual double StdDev(void) { return NAN; }
	};

	class IntegerDistribution : public Distribution {
	protected:
		int min;
		int max;
		vector<double> p;
		vector<double> f;
		double mu = NAN;
		double sigma = NAN;

		IntegerDistribution(int min, int max) : min(min), max(max), p(1 + max - min), f(1 + max - min) {}

	public:
		IntegerDistribution(int min, int max, const vector<double> & values) : min(min), max(max), p(1 + max - min), f(1 + max - min) {
			double *p = this->p.data();
			const double *pp = values.data();

			double sum = 0;
			double *f = this->f.data();

			for ( int i = min; i <= max; i++ ) {
				double prob = pp[i - min];

				assert_true(prob >= 0);

				p[i - min] = prob;
				sum += prob;

				f[i - min] = sum;
			}

			for ( int i = min; i <= max; i++ ) {
				p[i - min] /= sum;
				f[i - min] /= sum;
			}
		}

		template<typename T>
		IntegerDistribution(const Histogram<T> & hist) {
			min = hist.data.begin()->first;
			max = hist.data.rbegin()->first;
			p.resize(1 + max - min);
			f.resize(1+max - min);

			double *p = this->p.data();

			double sum = 0;
			double *f = this->f.data();

			for ( int i = min; i <= max; i++ ) {
				double prob = hist[i];

				assert_true(prob >= 0);

				p[i - min] = prob;
				sum += prob;

				f[i - min] = sum;
			}

			for ( int i = min; i <= max; i++ ) {
				p[i - min] /= sum;
				f[i - min] /= sum;
			}
		}

		virtual ~IntegerDistribution() {}

		virtual double Cdf(double t) {
			if ( t < min ) return 0;
			if ( t > max ) return 1;
			return f[(int) floor(t) - min];
		}

		virtual double Pdf(double t) {
			if ( t < min ) return 0;
			if ( t > max ) return 0;
			return p[(int) floor(t) - min];
		}

		virtual double InverseCdf(double t) {
			if ( t <= 0 ) return min;
			if ( t >= 1 ) return max;

			for ( uint i = 0; i < f.size(); i++ ) {
				if ( f[i] >= t ) {
					return i + min;
				}
			}

			return max;
		}

		virtual double Mean(void) {
			if ( std::isnan(mu) ) {
				double *p = this->p.data();
				double sum = 0;

				for ( int i = min; i <= max; i++ ) {
					sum += p[i - min] * i;
				}

				mu = sum;
			}

			return mu;
		}

		virtual double StdDev(void) {
			if ( std::isnan(sigma) ) {
				double *p = this->p.data();
				double sumSq = 0;
				double mu = Mean();

				for ( int i = min; i <= max; i++ ) {
					sumSq += p[i - min] * (i - mu) * (i - mu);
				}

				sigma = sqrt(sumSq);
			}

			return sigma;
		}

		double P(int i) const {
			assert_true(min <= i && i <= max);
			return p[i - min];
		}

		int Min() const { return min; }

		int Max() const { return max; }

		IntegerDistribution Add(const IntegerDistribution & other) {
			IntegerDistribution &my = *this;
			int newMin = my.min + other.min;
			int newMax = my.max + other.max;

			vector<double> newP(1 + newMax - newMin);

			for ( int i = min; i <= max; i++ ) {
				for ( int j = other.min; j <= other.max; j++ ) {
					newP[i + j - newMin] += P(i) * other.P(j);
				}
			}

			return IntegerDistribution(newMin, newMax, newP);
		}

		/// <summary>
		/// Emits the distribution to standard output in a tabular form.
		/// </summary>
		/// <param name="keyFormat">A function that converts the key to string format.</param>
		/// <param name="keyFormat">A function that converts the key to string format.</param>
		/// <returns>A function that converts the key to string format.</returns>

		ostream & Print(
			ostream & out,
			function<string(double)> valFormat
			) {
			double *p = this->p.data();
			out << "x" << "," << "f" << endl;

			for ( int i = min; i <= max; i++ ) {
				double f = p[i - min];

				if ( f > 0 ) {
					out << i << "," << valFormat(f) << endl;
				}
			}

			return out;
		}

		void Parse(istream & in) {
			typedef pair<int, double> P;

			p.clear();
			vector<P> input;

			CsvReader csv(in);
			vector<vector<string>> records;

			csv.Read(records);

			for ( auto record : records ) {
				if ( record.size() != 2 ) continue;

				if ( !isdigit(record[1][0]) ) continue;

				int key = atoi(record[0].c_str());
				double value = atof(record[1].c_str());

				assert_true(value >= 0);

				input.push_back(P(key, value));
			}

			sort(input.begin(), input.end(), [](P & x, P & y) { return x.first < y.first; });

			int newMin = input[0].first;
			int newMax = input.back().first;

			min = newMin;
			max = newMax;
			p.resize(1 + max - min);

			double sum = 0;

			for ( auto kvp : input ) {
				sum += kvp.second;
				p[kvp.first - min] = kvp.second;
			}

			for ( int i = min; i <= max; i++ ) {
				p[i - min] /= sum;
			}
		}

		/// <summary>
		///	Gets the distribution of the maximum of a subset containing 
		///	subsetSize items from the underlying set.
		/// </summary>

		IntegerDistribution GetMaximum(int subsetSize) {
			double *f = this->f.data();

			int newMin = max;
			int newMax = min;

			vector<double> Fm(1 + max - min);

			for ( int i = min; i <= max; i++ ) {
				// http://stats.stackexchange.com/questions/220/how-is-the-minimum-of-a-set-of-random-variables-distributed
				double F = f[i - min];
				Fm[i - min] = pow(F, subsetSize);
			}

			for ( int i = min; i <= max; i++ ) {
				if ( Fm[i - min] > 0 ) {
					newMin = i;
					break;
				}
			}

			for ( int i = max; i >= min; i-- ) {
				if ( Fm[i - min] > 0 ) {
					newMax = i;
					break;
				}
			}

			IntegerDistribution d(newMin, newMax);
			double *fMax = d.f.data();
			double *pMax = d.p.data();

			double fPrev = 0;

			for ( int i = newMin; i <= newMax; i++ ) {
				fMax[i - newMin] = Fm[i - min];
				pMax[i - newMin] = Fm[i - min] - fPrev;
				fPrev = Fm[i - min];
			}

			return d;
		}

		/// <summary>
		///	Gets the distribution of the maximum of a subset containing 
		///	subsetSize items from the underlying set.
		/// </summary>

		IntegerDistribution GetMinimum(int subsetSize) {
			double *f = this->f.data();

			int newMin = max;
			int newMax = min;

			vector<double> Fm(1 + max - min);

			for ( int i = min; i <= max; i++ ) {
				// http://stats.stackexchange.com/questions/220/how-is-the-minimum-of-a-set-of-random-variables-distributed
				double F = i == min ? 0 : f[i - 1 - min];
				double Fc = 1 - F;
				Fm[i - min] = 1 - pow(Fc, subsetSize);
			}

			for ( int i = min; i <= max; i++ ) {
				if ( Fm[i - min] > 0 ) {
					newMin = i;
					break;
				}
			}

			for ( int i = max; i >= min; i-- ) {
				if ( Fm[i - min] > 0 ) {
					newMax = i;
					break;
				}
			}

			IntegerDistribution d(newMin, newMax);
			double *fMax = d.f.data();
			double *pMax = d.p.data();

			double fPrev = 0;

			for ( int i = newMin; i <= newMax; i++ ) {
				fMax[i - newMin] = Fm[i - min];
				pMax[i - newMin] = Fm[i - min] - fPrev;
				fPrev = Fm[i - min];
			}

			return d;
		}

		/// <summary>
		///	Returns a distribution for a conditional event, as determined by the
		///	predicate, which must return true to indicate that the designated value
		///	satisfies the condition.
		/// The arguments passed to the predicate are: x, PDF(x) and CDF(x). 
		///	</summary>

		IntegerDistribution GetConditionalDistribution(function<bool(int, double, double)> predicate) {
			int newMin = max;
			int newMax = min;

			for ( int i = min; i <= max; i++ ) {
				if ( predicate(i, p[i - min], f[i - min]) ) {
					newMin = i;
					break;
				}
			}

			for ( int i = max; i >= min; i-- ) {
				if ( predicate(i, p[i - min], f[i - min]) ) {
					newMax = i;
					break;
				}
			}

			vector<double> newP(1 + newMax - newMin);

			for ( int i = newMin; i <= newMax; i++ ) {
				if ( predicate(i, p[i - min], f[i - min]) ) {
					newP[i - newMin] = p[i - min];
				}
			}

			return IntegerDistribution(newMin, newMax, newP);
		}
	};
}
