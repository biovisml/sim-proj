#pragma once
#include <set>
#include <iomanip>

#include "QutBio.h"
#include "DistanceType.hpp"
#include "KmerCodebook.hpp"

using namespace QutBio;
using namespace std;

namespace BuildCodebook {
	class Main {
	public:
		/// <summary> Main class contains the program entry point: Run().
		/// </summary>
		static void Run(int argc, char ** argv) {
			Args arguments(argc, argv);

			arguments.Show();

			if ( arguments.IsDefined("numThreads") ) {
				int numThreads;
				if ( arguments.Get("numThreads", numThreads) ) {
					omp_set_num_threads(numThreads);
				}
			}
			else {
				omp_set_num_threads(1);
			}

			try {
				Process(arguments);
			}
			catch ( Exception e ) {
				string message = e.what();

				if ( message.find("Safe to ignore") != 0 ) {
					throw e;
				}
			}
		}

	private:
		static vector<string> SelectionStrings() {
			static vector<string> vals {
				"Uniform",
				"LinearPerCluster",
				"LogPerCluster",
				""
			};

			return vals;
		}

		struct Params {
			string dbFile;
			string codebookFile;
			string matrixFile;

			int idIndex = 0;
			int classIndex = -1;
			int kmerLength = 0;
			size_t codebookSize = 0;

			DistanceType * dist = DistanceType::Blosum();
			int matrixId = 62;
			Alphabet * alphabet = Alphabet::AA();

			time_t seed = time(0);
			bool isCaseSensitive = false;

			KmerCodebook::InitMode initMode = KmerCodebook::InitMode::Uniform;
		};

		typedef double * pDouble;
		typedef double ** ppDouble;

		static void Process(Args arguments) {
			Params parms;
			GetValidatedParams(arguments, parms);

			cout << "--seed " << parms.seed << endl;

			UniformRealRandom rand((int) parms.seed);

			PointerList<FastaSequence> db;
			FastaSequence::ReadSequences(parms.dbFile, parms.idIndex, parms.classIndex, db);
			FastaSequence::PadSequences(db, parms.kmerLength, 'x');

			SimilarityMatrix * matrix = SimilarityMatrix::GetMatrix(parms.dist, parms.matrixId, parms.matrixFile, parms.isCaseSensitive);

			BlosumDifferenceFunction dist(matrix);
			KmerDistanceCache2 cache(parms.alphabet, &dist);

			for ( auto seq : db.Items() ) {
				seq->Encode(parms.alphabet, parms.kmerLength, cache.CharsPerWord());
			}

			time_t t0 = time(0);
			cerr << "Commencing codebook construction: " << t0 << endl;

			KmerCodebook codebook(cache, parms.kmerLength, parms.codebookSize, db, rand, false, parms.initMode);

			time_t t1 = time(0);
			cerr << "Finished codebook construction: " << t1 << endl;
			cerr << "Elapsed time: " << (t1 - t0) << "s" << endl;

			ofstream out(parms.codebookFile);
			codebook.Write(out);
			out.close();

			if ( parms.dist == DistanceType::Custom() ) {
				delete matrix;
			}
		}

		/// <summary> Parses and validates the arguments, returning the results in a Params object.
		/// </summary>
		/// <param name="arguments"></param>
		/// <returns></returns>

		static void GetValidatedParams(Args & arguments, Params & parms) {
			if ( !arguments.Get("dbFile", parms.dbFile) ) {
				cerr << "Argument 'dbFile' not defined." << endl;
				ShowHelp(); exit(1);
			}

			if ( !arguments.Get("codebookFile", parms.codebookFile) ) {
				cerr << "Argument 'codebookFile' not defined." << endl;
				ShowHelp(); exit(1);
			}

			if ( arguments.IsDefined("idIndex") ) {
				arguments.Get("idIndex", parms.idIndex);
			}

			if ( !(parms.idIndex >= 0) ) {
				cerr << "Argument 'idIndex' not valid." << endl;
				ShowHelp(); exit(1);
			}

			if ( arguments.IsDefined("classIndex") ) {
				arguments.Get("classIndex", parms.classIndex);
			};

			if ( !(parms.classIndex != parms.idIndex) ) {
				cerr << "Argument 'classIndex' must be different from 'idIndex'." << endl;
				ShowHelp(); exit(1);
			}

			if ( !(arguments.Get("codebookSize", parms.codebookSize) && parms.codebookSize > 0) ) {
				cerr << "Argument 'codebookSize' not supplied or not valid." << endl;
				ShowHelp(); exit(1);
			}

			if ( !(arguments.Get("kmerLength", parms.kmerLength)) ) {
				cerr << "Argument 'kmerLength' not valid." << endl;
				ShowHelp(); exit(1);
			}

			if ( !(parms.kmerLength > 0) ) {
				cerr << "Argument 'kmerLength' not valid." << endl;
				ShowHelp(); exit(1);
			}

			if ( !(arguments.Get("dist", DistanceType::Values(), parms.dist)) ) {
				cerr << "Argument 'dist' not valid." << endl;
				ShowHelp(); exit(1);
			}

			if ( arguments.IsDefined("matrixId") ) {
				if ( !(arguments.Get("matrixId", parms.matrixId)) ) {
					cerr << "Argument 'matrixId' not valid." << endl;
					ShowHelp(); exit(1);
				}
			}
			else {
				parms.matrixId = 62;
			}

			if ( parms.dist == DistanceType::Custom() ) {
				if ( !arguments.IsDefined("matrixFile") ) {
					cerr << "Argument 'matrixFile' not supplied for custom similarity matrix." << endl;
					ShowHelp(); exit(1);
				}
				else {
					arguments.Get("matrixFile", parms.matrixFile);
				}
			}
			else {
				vector<int> matrices{35, 40, 45, 50, 62, 80, 100};

				bool found = false;

				for ( auto x : matrices ) {
					if ( x == parms.matrixId ) { found = true; }
				}

				if ( !found ) {
					cerr << "Matrix id not recognised." << endl;
					ShowHelp(); exit(1);
				}
			}

			if ( arguments.IsDefined("alphabet") ) {
				if ( !arguments.Get("alphabet", Alphabet::Values(), parms.alphabet) ) {
					cerr << "Unable to parse argument 'alphabet'." << endl;
					ShowHelp(); exit(1);
				}
			}
			else {
				parms.alphabet = Alphabet::AA();
			}

			if ( arguments.IsDefined("seed") ) {
				int seed;

				if ( !(arguments.Get("seed", seed)) ) {
					cerr << "Argument 'seed' not valid." << endl;
					ShowHelp(); exit(1);
				}

				if ( seed == -1 ) {
					seed = (int) time(NULL);
				}

				parms.seed = seed;
			}

			if ( arguments.IsDefined("isCaseSensitive") ) {
				if ( !(arguments.Get("isCaseSensitive", parms.isCaseSensitive)) ) {
					cerr << "Argument 'isCaseSensitive' not valid." << endl;
					ShowHelp(); exit(1);
				}
			}

			if ( arguments.IsDefined("initMode") ) {
				string initMode;

				if ( !(arguments.Get("initMode", initMode)) ) {
					cerr << "Argument 'initMode' not valid." << endl;
					ShowHelp(); exit(1);
				}

				auto selectionStrings = SelectionStrings();

				try {
					for ( int i = 0; selectionStrings[i] != ""; i++ ) {
						if ( selectionStrings[i] == initMode ) {
							parms.initMode = (KmerCodebook::InitMode) i;
							throw 0;
						}
					}

					cerr << "Argument 'initMode' not valid." << endl;
					ShowHelp(); exit(1);
				}
				catch (...) {
					// This means its OK.
				}
			}

		}

		/// <summary> Displays general help.
		/// </summary>

		static void ShowHelp() {
			string helpText =
				"KmerQuery:"  "\n"
				"----------"  "\n"
				"Computes the document ranking of a query dataset under the two - level hierarchical kmer similarity measure."  "\n"
				"----------"  "\n"
				"Required arguments:"  "\n"
				"-- dbFile <fileName>              The name of a FASTA formatted file containing the database to be queried."  "\n"
				"-- codebookFile <fileName>: The name of a CSV formatted file that will be overwritten with a"  "\n"
				"                           list of records. Each record defines one cluster. The first column" "\n"
				"                           is the geneId of the prototype for the cluster. The remaining"  "\n"
				"                           columns are the geneId's of database sequences which map to the"  "\n"
				"                           prototype under a nearest-neighbour search."  "\n"
				"-- kmerLength <uint>		    The number of characters in the kmer tiling."  "\n"
				"-- dist( UngappedEdit | Blosum ) The distance measure to use."  "\n"
				"-- fragLength <uint>          The size to dice each record."  "\n"
				"-- interval <uint>            The interval between fragments. Should normally be less than or equal to the fragment length."  "\n"
				"-- fragMode( BestOfBest | Hausdorff | HausdorffAverage ) The combination mode used to aggregate fragment scores."  "\n"
				"-- kmerMode( BestOfBest | Hausdorff | HausdorffAverage ) The combination mode used to aggregate fragment scores."
				"Optional arguments :"  "\n"
				"--matrixId: 35 | 40 | 45 | 50 | 62 | 80 | 100 - The blosum matrix to use, if \"--dist Blosum\" is set."  "\n"
				"             Default value is 62."  "\n"
				"--idIndex <uint> - The zero-origin index position of the ID field in the pipe-separated FASTA definition line."  "\n"
				"	Default value is 0;"  "\n"
				"--classIndex <uint>          The zero - origin index position of the class label field in the pipe - separated FASTA definition line."  "\n"
				"	Default value is 1;"  "\n"
				"--alphabet( AA | DNA )			The alphabet from which symbols are assumed to be drawn. Default is Amino Acid( AA )."  "\n"
				"--initMode: Uniform | LinearPerCluster | LogPerCluster - The initMode mode used to obtain initial" "\n"
				"             prototypes for the codebook."  "\n"
				"             *  Uniform initMode samples the distinct kmer vocabulary without reference to class"  "\n"
				"             *  LinearPerCluster initMode selects a number for each class proportional to the"  "\n"
				"                number of distinct kmers in the class."  "\n"
				"             *  LogPerCluster initMode selects a number for each class proportional to the"  "\n"
				"                logarithm of number of distinct kmers in the class."  "\n"
				"             In both the stratified modes, at least one kmer is selected for each class, even if"  "\n"
				"             this means the codebook size inflates slightly to accommodate them. This is"  "\n"
				"             predictable: ceil is used to calculate proportional populations."  "\n"
				;

			cout << helpText;
		}
	};

}


