#include "Main.hpp"

using namespace BuildCodebook;

int main( int argc, char** argv ) {
	try {
		Main::Run( argc, argv );
	}
	catch ( Exception ex ) {
		cerr << "Un-handled exception : " << ex.what() << " - " << ex.File() << "(" << ex.Line() << ")" << endl;
	}
	catch ( runtime_error & err ) {
		cerr << "Un-handled exception:" << endl << err.what() << endl;
	}
	return 0;
}

mutex FragmentAggregationMode::m;
mutex QutBio::DistanceType::m;
